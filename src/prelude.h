
/// @file prelude.h
/// The "prelude" is a set of common definitions shared among other files.

#pragma once

#include <map>
#include <set>
#include <vector>
#include <memory>
#include <string>
#include <chrono>
#include <typeinfo>
#include <iostream>
#include <cinttypes>
#include <algorithm>
#include <functional>
#include <initializer_list>


/// A function marked for removal in future versions.
/// The GCC function attribute `__deprecated__` is applied.
/// For an unknown reason, `__attribute__(( __deprecated__ ))` is unavailable
/// when `SDL2/SDL_ttf.h` is inluded.
#define deprecated     __attribute__(( __deprecated__ ))

// We also have to redefine SDL's macro to avoid clashes
#ifdef SDL_DEPRECATED
#undef SDL_DEPRECATED
#endif

#define SDL_DEPRECATED __attribute__(( __deprecated__ ))

/// A function with a significant return value.
/// A function is deemed important if neglecting its return value is likely to lead to an error.
/// The GCC function attribute `__warn_unused_result__` is applied.
#define important      __attribute__(( __warn_unused_result__ ))

/// A rarely used function.
/// A function marked as cold is unlikely to be called often. Use mainly for initializers.
/// The compiler won't try to optimise this function very hard, and will put it in a section
/// with other cold functions. It is likely to be mis-aligned.
/// The GCC function attribute `__cold__` is applied.
#define cold           __attribute__(( __cold__ ))

/// A frequently used function.
/// A function marked as hot is an execution hotspot. Use for critical functions executed
/// every frame. The compiler will try to optimise this function harder than others.
/// The GCC function attribute `__hot__` is applied.
#define hot            __attribute__(( __hot__ ))

#define QUOTE( x ) #x

#define let_mut_helper( __x, __y ) let_mut_inner_helper( __x, __y )
#define let_mut_inner_helper( __line, __id ) __unused_const__ ## __line ## _ ## __id

#define func auto
#define let_mut auto
#define let const auto
#define mut __attribute__(( unused )) let_mut_helper( __LINE__, __COUNTER__ ) = 0; auto

/// A    signed integer of at least 8 bit width (fastest available).
typedef  int_fast8_t   i8;
/// An unsigned integer of at least 8 bit width (fastest available).
typedef uint_fast8_t   u8;
/// A    signed integer of at least 16 bit width (fastest available).
typedef  int_fast16_t i16;
/// An unsigned integer of at least 16 bit width (fastest available).
typedef uint_fast16_t u16;
/// A    signed integer of at least 32 bit width (fastest available).
typedef  int_fast32_t i32;
/// An unsigned integer of at least 32 bit width (fastest available).
typedef uint_fast32_t u32;
/// A    signed integer of at least 64 bit width (fastest available).
typedef  int_fast64_t i64;
/// An unsigned integer of at least 64 bit width (fastest available).
typedef uint_fast64_t u64;

/// A    signed integer of exactly 8 bit width.
typedef  int8_t   ie8;
/// An unsigned integer of exactly 8 bit width.
typedef uint8_t   ue8;
/// A    signed integer of exactly 16 bit width.
typedef  int16_t ie16;
/// An unsigned integer of exactly 16 bit width.
typedef uint16_t ue16;
/// A    signed integer of exactly 32 bit width.
typedef  int32_t ie32;
/// An unsigned integer of exactly 32 bit width.
typedef uint32_t ue32;
/// A    signed integer of exactly 64 bit width.
typedef  int64_t ie64;
/// An unsigned integer of exactly 64 bit width.
typedef uint64_t ue64;

/// A 32-bit floating point number.
typedef float  f32;
/// A 64-bit floating point number.
typedef double f64;

using namespace std;

let inf = 1.0f / 0.0f;

/// Get the time since the start of the program in milliseconds.
/// @param offset Offset the result by this number
inline i64 time( i64 offset = 0 ) {
	using namespace std::chrono;

	milliseconds ms = duration_cast<milliseconds>(
		system_clock::now().time_since_epoch()
	);

	return (i64)ms.count() + offset;
}
