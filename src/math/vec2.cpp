
#include "math/vec2.h"

const vec2 vec2::zero  = vec2(  0.0f,  0.0f );
const vec2 vec2::left  = vec2( -1.0f,  0.0f );
const vec2 vec2::right = vec2(  1.0f,  0.0f );
const vec2 vec2::up    = vec2(  0.0f, -1.0f );
const vec2 vec2::down  = vec2(  0.0f,  1.0f );

vec2::vec2( f32 _x, f32 _y ) noexcept
: x( _x ), y( _y ) {}

vec2::vec2( u32 _x, u32 _y ) noexcept
: x( (f32)_x ), y( (f32)_y ) {}

vec2::vec2( initializer_list<f32> values )
: vec2( *( values.begin() ), *( values.begin() + 1 ) ) {}

ostream& operator<<( ostream& stream, const vec2 v ) {
	return stream << "(" << v.x << ", " << v.y << ")";
}

vec2 vec2::normal() const {
	return normalise();
}

vec2 vec2::normalise() const {
	let mag = magnitude();

	if ( mag != 0.0f ) {
		return vec2( x / mag, y / mag );
	}

	return vec2::zero;
}

vec2 vec2::normalize() const {
	return normalise();
}

vec2  vec2::operator- () const noexcept {
	return vec2( -x, -y );
}

vec2  vec2::operator- ( const vec2& other ) const noexcept {
	return vec2( x - other.x, y - other.y );
}

vec2& vec2::operator-=( const vec2& other ) noexcept {
	x -= other.x;
	y -= other.y;

	return *this;
}

vec2  vec2::operator+ () const noexcept {
	return *this;
}

vec2  vec2::operator+ ( const vec2& other ) const noexcept {
	return vec2( x + other.x, y + other.y );
}

vec2& vec2::operator+=( const vec2& other ) noexcept {
	x += other.x;
	y += other.y;

	return *this;
}

vec2  vec2::operator/ ( const f32& n ) const {
	return vec2( x / n, y / n );
}

vec2  vec2::operator* ( const f32& n ) const noexcept {
	return vec2( x * n, y * n );
}

vec2& vec2::operator*=( const f32& n ) noexcept {
	x *= n;
	y *= n;

	return *this;
}
