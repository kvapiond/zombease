
#pragma once

#include "prelude.h"

#include "utils/logger.h"
#include "math/vec2.h"

namespace Geometry {
	f32 get_lines_intersection_fraction( vec2 v11, vec2 v12, vec2 v21, vec2 v22, vec2& normal );

	inline f32 get_lines_intersection_fraction( vec2 v11, vec2 v12, vec2 v21, vec2 v22 ) {
		vec2 normal;
		return get_lines_intersection_fraction(
			v11, v12, v21, v22, normal
		);
	}
}
