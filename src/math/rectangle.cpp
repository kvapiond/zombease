
#include "math/rectangle.h"

const Rectangle Rectangle::zero = Rectangle( vec2::zero, vec2::zero );

Rectangle::Rectangle( const vec2& pos, const vec2& dimensions )
: x( pos.x ), y( pos.y )
, w( dimensions.x ), h( dimensions.y )
, x2( pos.x + dimensions.x ), y2( pos.y + dimensions.y ) {}

Rectangle::Rectangle( const vec2& pos, f32 _w, f32 _h )
: x( pos.x ), y( pos.y ), w( _w ), h( _h ), x2( pos.x + _w ), y2( pos.y + _h ) {}

Rectangle::Rectangle( f32 _x, f32 _y, f32 _w, f32 _h )
: x( _x ), y( _y ), w( _w ), h( _h ), x2( _x + _w ), y2( _y + _h ) {}

 Rectangle Rectangle::from_points( vec2 a, vec2 b ) {
	return Rectangle( a, b.x - a.x, b.y - a.y );
}

void Rectangle::closest_edge_projection( vec2 point, vec2& pen ) const {
	f32 min_distance = abs( x - point.x );
	pen = vec2( -x, -point.y );

	f32 distance;
	if ( ( distance = abs( x2 - point.x ) ) < min_distance ) {
		min_distance = distance;
		pen = vec2( -x2, -point.y );
	}

	if ( ( distance = abs( y2 - point.y ) ) < min_distance ) {
		min_distance = distance;
		pen = vec2( -point.x, -y2 );
	}

	if ( abs( y - point.x ) < min_distance ) {
		pen = vec2( -point.x, -y );
	}
}

f32 Rectangle::area() const {
	return w * h;
}

Rectangle Rectangle::overlap( const Rectangle& other ) const {
	let a = vec2( max( x,  other.x  ), max( y,  other.y  ) );
	let b = vec2( min( x2, other.x2 ), min( y2, other.y2 ) );

	if ( a.x > b.x || a.y > b.y ) {
		return Rectangle::zero;
	}

	return from_points( a, b );
}

bool Rectangle::contains( vec2 point ) const {
	return point.x > x && point.x < x2 && point.y > y && point.y < y2;
}

bool Rectangle::intersects( const Rectangle& other ) const {
	return minkowski( *this, other ).contains( vec2::zero );
}

Rectangle Rectangle::minkowski( const Rectangle& a, const Rectangle& b ) {
	return Rectangle( a.x - b.x2, a.y - b.y2, a.w + b.w, a.h + b.h );
}

void Rectangle::pretty_print( ostream& stream ) const {
	stream << "Rectangle( " << x << ", " << y << ", " << w << ", " << h << ")";
}

Rectangle Rectangle::operator+( const Rectangle& other ) const {
	return Rectangle( x + other.x, y + other.y, w + other.w, h + other.h );
}

Rectangle::operator SDL_Rect() const {
	SDL_Rect rect;

	rect.x = (int)x;
	rect.y = (int)y;
	rect.w = (int)w;
	rect.h = (int)h;

	return rect;
}
