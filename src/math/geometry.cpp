
#include "math/geometry.h"

f32 Geometry::get_lines_intersection_fraction( vec2 v11, vec2 v12, vec2 v21, vec2 v22, vec2& normal ) {
	let s = v12 - v11;
	let t = v22 - v21;

	normal.x = -t.y;
	normal.y = +t.x;

	normal = normal.normalise();

	let origin_diff = v21 - v11;

	let num = origin_diff.cross( s );
	let den = s.cross( t );

	if ( num == 0.0f ) {
		return inf;
	}

	let p1 = num / den;
	let p2 = origin_diff.cross( t ) / den;

	if ( p1 >= 0.0f && p1 <= 1.0f && p2 >= 0.0f && p2 <= 1.0f ) {
		return p2;
	}

	return inf;
}
