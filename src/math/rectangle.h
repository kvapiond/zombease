
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"
#include "math/vec2.h"
#include "utils/pretty.h"

/// (Surprisingly) a rectangle.
struct Rectangle : public Pretty {
	/// Zero-width, zero-height Rectangle located at vec2::zero.
	static const Rectangle zero;

	f32 x;
	f32 y;
	f32 w;
	f32 h;
	f32 x2;
	f32 y2;

	Rectangle( const Rectangle& other ) = default;
	Rectangle( const vec2& pos, const vec2& dimensions );
	Rectangle( const vec2& pos, f32 _w, f32 _h );
	Rectangle( f32 _x, f32 _y, f32 _w, f32 _h );

	inline Rectangle(  u32 _x,  u32 _y,  u32 _w,  u32 _h )
	     : Rectangle( (f32)_x, (f32)_y, (f32)_w, (f32)_h ) {}

	~Rectangle() = default;

	static Rectangle from_points( vec2 a, vec2 b );

	f32 area() const;

	/// Get the Rectangle of the overlapping area of two rectangles.
	/// @return A new Rectangle or Rectangle::zero, in case the two do not overlap.
	Rectangle overlap( const Rectangle& other ) const;

	/// Project the given point (inside the Rectangle) to the closest edge.
	/// @param[in] point The point to project
	/// @param[out] pen The penetration vector
	void closest_edge_projection( vec2 point, vec2& pen ) const;

	/// Check whether the given point lies within this Rectangle.
	bool contains( vec2 point ) const;

	/// Check whether this Rectangle intersects another.
	bool intersects( const Rectangle& other ) const;

	/// Create a Rectangle as a [Minkowski difference](https://en.wikipedia.org/wiki/Minkowski_addition)
	/// of two existing rectangles.
	static Rectangle minkowski( const Rectangle& a, const Rectangle& b );

	void pretty_print( ostream& stream ) const override;

	/// Add a Rectangle to this one.
	/// This is simple piecewise addition, which means you can reposition a Rectangle
	/// and scale it at the same time by using this operator.
	Rectangle operator+( const Rectangle& other ) const;

	/// Convert this Rectangle to an SDL_Rect struct.
	operator SDL_Rect() const;
};
