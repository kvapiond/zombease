
#pragma once

#include <cmath>

#include "prelude.h"

/// A 2-dimensional vector backed up by ::f32.
struct vec2 {
	/// The zero vector (0, 0).
	static const vec2 zero;

	/// The left (screen space) vector (-1, 0).
	static const vec2 left;
	/// The right (screen space) vector (1, 0).
	static const vec2 right;
	/// The up (screen space) vector (0, -1).
	static const vec2 up;
	/// The down (screen space) vector (0, 1).
	static const vec2 down;

	f32 x = 0.0f;
	f32 y = 0.0f;

	vec2() noexcept = default;
	vec2( vec2&& other ) noexcept = default;
	vec2( const vec2& other ) noexcept = default;

	vec2( f32 _x, f32 _y ) noexcept;
	vec2( u32 _x, u32 _y ) noexcept;
	deprecated vec2( initializer_list<f32> values );
	~vec2() = default;

	inline f32 magnitude() const noexcept {
		return hypot( x, y );
	}

	inline f32 dot( const vec2& other ) const noexcept {
		return x * other.x + y * other.y;
	}

	inline f32 cross( const vec2& other ) const noexcept {
		return x * other.y - y * other.x;
	}

	vec2 normal() const;
	vec2 normalise() const;
	/// Alias to vec2::normalise().
	/// @deprecated Use proper English instead.
	vec2 deprecated normalize() const;

	vec2  operator- () const noexcept;
	vec2& operator-=( const vec2& other ) noexcept;
	vec2  operator- ( const vec2& other ) const noexcept;

	vec2  operator+ () const noexcept;
	vec2& operator+=( const vec2& other ) noexcept;
	vec2  operator+ ( const vec2& other ) const noexcept;

	vec2& operator*=( const f32& n ) noexcept;
	vec2  operator* ( const f32& n ) const noexcept;

	vec2  operator/ ( const f32& n ) const;

	vec2& operator=( const vec2& other ) noexcept = default;

	inline bool operator==( const vec2& other ) const noexcept {
		return x == other.x && y == other.y;
	}

	inline bool operator!=( const vec2& other ) const noexcept {
		return x != other.x || y != other.y;
	}

	inline f32 operator[]( u64 i ) const noexcept {
		if ( i == 0 ) {
			return x;
		}

		return y;
	}

	inline f32& operator[]( u64 i ) noexcept {
		if ( i == 0 ) {
			return x;
		}

		return y;
	}

	friend ostream& operator<<( ostream& stream, const vec2 v );
};

inline vec2 operator*( const f32& n, const vec2& v ) noexcept {
	return v * n;
}
