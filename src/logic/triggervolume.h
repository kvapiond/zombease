
#pragma once

#include "prelude.h"

#include "math/rectangle.h"
#include "logic/invisibledynamic.h"

/// An InvisibleDynamic object which calls a function when something collides with it.
/// Typically used for invisible triggers, such as entering a new area.
class TriggerVolume : public InvisibleDynamic {
public:
	TriggerVolume( const Rectangle& rect );
	virtual ~TriggerVolume() = default;

	/// Set the function to call when this TriggerVolume is triggered.
	virtual void set_callback( function<void( TriggerVolume&, shared_ptr<GameObject> )> _callback );

	/// Trigger this TriggerVolume.
	/// @param obj The object which caused the trigger
	virtual void trigger( shared_ptr<GameObject> obj );

protected:
	function<void( TriggerVolume&, shared_ptr<GameObject> )> callback;
};
