
#include "logic/triggervolume.h"

TriggerVolume::TriggerVolume( const Rectangle& rect )
: InvisibleDynamic( rect ) {
	is_solid = true;
}

void TriggerVolume::set_callback( function<void( TriggerVolume&, shared_ptr<GameObject> )> _callback ) {
	callback = _callback;
}

void TriggerVolume::trigger( shared_ptr<GameObject> obj ) {
	callback( *this, obj );
}
