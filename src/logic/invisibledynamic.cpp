
#include "logic/invisibledynamic.h"

InvisibleDynamic::InvisibleDynamic( const Rectangle& rect )
: Dynamic( rect ) {}

bool InvisibleDynamic::draw( Graphics::Renderer& ) const {
	return true;
}

bool InvisibleDynamic::draw( Graphics::Renderer&, vec2 ) const {
	return true;
}
