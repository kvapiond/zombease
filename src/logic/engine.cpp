
#include "logic/engine.h"

Engine Engine::instance( 2048, 2048 );

Engine::Engine( u32 world_width, u32 world_height )
: window( nullptr ), heart_texture( nullptr ), font( nullptr ), world( world_width, world_height ) {
	last_time = time();
}

Engine::~Engine() {
	IMG_Quit();
	SDL_Quit();

	// Explicitly destroy the font, so we can quit the TTF library
	font.~Font();

	TTF_Quit();
}

Engine& Engine::get_instance() {
	return Engine::instance;
}

bool Engine::init() {
	SCP();
	using namespace Graphics;

	Logger::log << "Initialising SDL";
	if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
		Logger::line() << ERROR << "Failed to initialize SDL2:" << SDL_GetError() << END_SCOPE;
		return false;

	} else {
		Logger::log << "Creating a window";
		window = Window( "yeey", Rectangle( 10, 10, window_w, window_h ) );

		Logger::log << "Creating renderer";
		Renderer::main = Renderer( window, false );
		SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "nearest" );
		Renderer::main.set_draw_blend_mode( SDL_BLENDMODE_BLEND );

		if ( !( IMG_Init( IMG_INIT_PNG ) & IMG_INIT_PNG ) ) {
			Logger::line() << ERROR << "Failed to initialize SDL_Image:" << IMG_GetError() << END_SCOPE;
			return false;
		}

		if ( TTF_Init() < 0 ) {
			Logger::line() << "Failed to initialize SDL_ttf:" << TTF_GetError() << END_SCOPE;
			return false;
		}
	}

	Logger::log << "Instantiating texture atlas";
	atlas = shared_ptr<TextureAtlas>( new TextureAtlas( Renderer::main ) );

	Logger::log << "Instantiating the player";

	let tex1 = atlas->load_texture( "src/assets/human.png" );
	let tex2 = atlas->load_texture( "src/assets/zombie.png" );

	let mut player_sprites = shared_ptr<Spritesheet>(
		new Spritesheet( "player_sprites", tex1 )
	);

	for ( u8 i = 0; i < 10; i++ ) {
		if ( !world.add( shared_ptr<Dynamic>(
			new Zombie( 10.0f, vec2( 200.0f + 200.0f * i, 300.0f ), shared_ptr<Spritesheet>( new Spritesheet( "z", tex2 ) ) )
		) ) ) {
			Logger::log << ERROR << "Unable to spawn zombie";
			return false;
		}
	}

	player = shared_ptr<Player>( new Player( 10.0f, vec2( 15.0f, 15.0f ), player_sprites ) );
	if ( !world.add( static_cast<shared_ptr<Dynamic>>( player ) ) ) {
		Logger::log << ERROR << "Unable to spawn player";
		return false;
	}

	Logger::log << "Loading font";

	font = Font::load( "src/assets/IndieFlower.ttf", 72 );
	heart_texture = atlas->load_texture( "src/assets/heart.png" );
	HealthPack::heart = heart_texture;

	load_stats();

	Logger::log << END_SCOPE;
	return true;
}

bool Engine::frame() {
	i64 now = time();
	i64 delta_time = now - last_time;

	if ( !handle_event() ) {
		return false;
	}

	using namespace Graphics;
	Renderer::main.clear( Colour::white );

	set<shared_ptr<Dynamic>> dead;

	for ( let elem : overlay_elements ) {
		elem->draw( Renderer::main );
	}

	if ( game_state == GameState::Menu ) {
		print_text( vec2( -1.0f, (f32)window_h / 2.0f - 100 ), "Play" );
		print_text( vec2( -1.0f, (f32)window_h / 2.0f       ), "Stats" );
		print_text( vec2( -1.0f, (f32)window_h / 2.0f + 100 ), "Quit" );

	} else if ( game_state == GameState::GameOver ) {
		print_text( vec2( -1.0f, -1.0f ), "Game over!", Colour::red );
		stringstream ss;
		ss << "Zombies killed: " << Zombie::kill_count;
		print_text( vec2( -1.0f, 800 ), ss.str() );

	} else if ( game_state == GameState::StatsScreen ) {
		print_text( vec2( -1.0f,   50.0f ), "Statistics" );
		print_text( vec2(  250.0f, 150.0f ), "Total Kills   Time Survived" );

		Colour c = Colour( 0x67, 0x00, 0xcb, 0xff );
		Colour d = Colour::black;

		u8 pos = 0;
		for ( let stat_entry : stats ) {
			if ( pos >= MAX_STATS_ENTRIES ) { break; }

			stringstream ss;
			ss << stat_entry.first;
			print_text( vec2( 300.0f, 200.0f + pos * 50.0f ), ss.str(), pos % 2 == 0 ? c : d );
			ss = stringstream();
			ss << floor( (f32)stat_entry.second / 100 ) / 10 << " s";
			print_text( vec2( 700.0f, 200.0f + pos * 50.0f ), ss.str(), pos % 2 == 1 ? c : d );

			pos++;
		}

	} else if ( game_state == GameState::InGame ) {
		for ( let& drawable : world.get_drawables() ) {
			drawable->draw( Renderer::main );
		}

		for ( let& dynamic : world.get_dynamics() ) {
			if ( !dynamic->update( *this, delta_time ) ) {
				dead.insert( dynamic );
			}
		}

		if ( debug_highlights ) {
			world.render_debug_overlay();
		}

		if ( debug_highlights ) {
			/// @debug
		}

		for ( u32 i = 0; i < (u32)( player->get_health() / 2 ); i++ ) {
			Renderer::main.draw_texture( vec2( (f32)i * 32.0f, 0.0f ), heart_texture );
		}

		if ( !player->alive() ) {
			game_state = GameState::GameOver;
			// Save stats
			stats.push_back( make_pair( Zombie::kill_count, time() - game_started ) );
		}
	}

	Renderer::main.present();

	if ( !dead.empty() ) {
		if ( !world.remove( dead ) ) {
			Logger::log << WARN << "Removing dead objects failed";
		}
	}

	last_time = now;
	return true;
}

World& Engine::get_world() {
	return world;
}

shared_ptr<TextureAtlas> Engine::get_atlas() {
	return atlas;
}

/// @todo Multiplayer!
vector<shared_ptr<Player>> Engine::get_players() {
	return { player };
}

bool Engine::held( SDL_Keycode keycode ) const {
	let iter = held_keys.find( keycode );

	if ( iter == held_keys.end() || !iter->second ) {
		return false;
	}

	return true;
}

void Engine::load_stats() {
	SCP();
	fstream file;

	Logger::log << "Opening save file";
	file.open( SAVE_PATH, ios::in );

	if ( !file.is_open() ) {
		Logger::log << ERROR << "Could not open file for reading" << END_SCOPE;
		return;
	}

	i64 time;
	u32 kills;
	Logger::log << "Reading data";
	for ( u8 i = 0; i < MAX_STATS_ENTRIES; i++ ) {
		if ( !file.good() ) {
			Logger::log << "File is no longer valid, stopping";
			break;
		}

		file >> kills >> time;
		if ( file.fail() ) {
			Logger::log << ERROR << "Reading failed" << END_SCOPE;
			return;
		}

		stats.push_back( make_pair( kills, time ) );
	}

	file.close();

	Logger::log << END_SCOPE;
}

void Engine::save_stats() const {
	SCP();
	fstream file;

	Logger::log << "Opening save file";
	file.open( SAVE_PATH, ios::out );

	if ( !file.is_open() ) {
		Logger::log << ERROR << "Could not open file for writing" << END_SCOPE;
		return;
	}

	u8 i = 0;
	Logger::log << "Saving data";
	for ( let stat_entry : stats ) {
		if ( i++ >= MAX_STATS_ENTRIES ) { break; }
		file << stat_entry.first << "\t" << stat_entry.second << endl;
	}

	file.close();

	Logger::log << END_SCOPE;
}

void Engine::print_text( vec2 pos, const string& str, Graphics::Colour colour ) {
	using namespace Graphics;

	Texture text = Renderer::main.create_texture_from_surface( font.render_utf8_blended( str, colour ) );

	if ( pos.x == -1.0f ) {
		pos.x = (f32)window_w / 2 - text.get_dim().x / 2;
	}

	if ( pos.y == -1.0f ) {
		pos.y = (f32)window_h / 2 - text.get_dim().y / 2;
	}

	Renderer::main.draw_texture( pos, text );
}

bool Engine::handle_event() {
	SDL_Event event;
	if ( !SDL_PollEvent( &event ) ) {
		SDL_Event e;

		SDL_PushEvent( &e );
	}

	shared_ptr<Dynamic> bullet;

	vec2 touch;
	SDL_Keycode sym;
	switch ( event.type ) {
		case SDL_QUIT:
			return false;

		case SDL_KEYDOWN:
			held_keys[ event.key.keysym.sym ] = true;
			break;

		case SDL_KEYUP:
			sym = event.key.keysym.sym;

			if ( sym == SDLK_q ) {
				return false;

			} else if ( sym == SDLK_g && held_keys[ SDLK_g ] ) {
				debug_highlights = !debug_highlights;

			} else if ( sym == SDLK_c && held_keys[ SDLK_c ] ) {
				/// @todo @debug
			}

			held_keys[ sym ] = false;
			break;

		case SDL_MOUSEBUTTONDOWN:
			held_buttons[ event.button.button ] = true;
			break;

		case SDL_MOUSEBUTTONUP:
			touch = vec2( (f32)event.button.x, (f32)event.button.y ) + camera;

			if ( event.button.button == SDL_BUTTON_LEFT && held_buttons[ event.button.button ] ) {
				switch ( game_state ) {
					case GameState::InGame:
						// Shoot!
						bullet = shared_ptr<Dynamic>( new Bullet(
							4.0f,
							player->get_pos() + player->get_dim() / 2,
							( touch - player->get_pos() - player->get_dim() / 2 ).normal(),
							player,
							shared_ptr<Spritesheet>( new Spritesheet( "foo", atlas->load_texture( "src/assets/bullet.png" ) ) )
						) );

						if ( !world.add( bullet ) ) {
							Logger::log << ERROR << "Bullet spawning failed!";
						}

						break;

					case GameState::Menu:
						if ( touch.x > 500 && touch.x < 700 && touch.y > 350 && touch.y < 620 ) {
							touch.y -= 350;

							if ( touch.y < 90 ) {
								game_state = GameState::InGame;

								player->set_health( 10.0f );

								game_started = time();

							} else if ( touch.y < 180 ) {
								game_state = GameState::StatsScreen;

							} else {
								return false;
							}
						}
						break;

					case GameState::GameOver:
						game_state = GameState::Menu;
						break;

					case GameState::StatsScreen:
						game_state = GameState::Menu;
						break;

					default:
						break;
				}
			}

			held_buttons[ event.button.button ] = false;
			break;

		default:
			break;
	}

	return true;
}
