
#pragma once

#include "prelude.h"
#include "physics/dynamic.h"
#include "logic/gameobject.h"
#include "entities/drawable.h"
#include "graphics/renderer.h"

class WorldChunk;
#include "logic/worldchunk.h"

/// The game world.
/// Takes care of splitting the world into multiple chunks,
/// to reduce overhead of collision detection and speed up
/// queries for on-screen objects.
class World {
public:
	World( u32 _w, u32 _h, u32 _chunk_size = 256 );

	/// Remove multiple Drawable objects from this world at once.
	/// @return Wether the removal was successful (objects found and removed).
	bool remove( set<shared_ptr<Drawable>> objs ) important;

	/// Remove multiple Dynamic objects from this world at once.
	/// @return Wether the removal was successful (objects found and removed).
	bool remove( set<shared_ptr<Dynamic>> objs ) important;

	/// Remove a Drawable from this world.
	/// @return Whether the removal was successful (object found and removed).
	bool remove( shared_ptr<Drawable> obj ) important;

	/// Remove a Dynamic from this world.
	/// @return Whether the removal was successful (object found and removed).
	bool remove( shared_ptr<Dynamic> obj ) important;

	/// Add a Drawable to this world.
	/// @return Whether the addition was successful.
	bool add( shared_ptr<Drawable> obj ) important;

	/// Add a Dynamic to this world.
	/// @return Whether the addition was successful.
	bool add( shared_ptr<Dynamic> obj ) important;

	/// Render an overview of the world chunks for debugging purposes.
	void render_debug_overlay() const;

	/// Find nearby objects.
	/// @param obj The object to search around
	/// @return Dynamic objects found in this chunk or one of its 8 (or fewer) neighbouring chunks.
	vector<shared_ptr<Dynamic>> get_nearby( shared_ptr<Dynamic> obj ) const hot;

	/// Find chunks surrounding another chunk.
	/// @param chunk The chunk to search around
	/// @param include_self Whether to include the centre chunk in the results
	vector<shared_ptr<WorldChunk>> get_nearby_chunks( shared_ptr<WorldChunk> chunk, bool include_self = false ) const hot;

	/// Find chunks containing an object.
	/// @param obj The GameObject to search for
	/// @return All chunks which contain the GameObject (could be one or more, the object can be located
	///         at a chunk boundary or span several chunks due to large dimensions).
	vector<shared_ptr<WorldChunk>> get_overlapping_chunks( const GameObject& obj ) const;

	/// Get all Drawables registered within this world.
	vector<shared_ptr<Drawable>>& get_drawables();

	/// Get all Dynamics registered within this world.
	vector<shared_ptr<Dynamic>>& get_dynamics();

private:
	u32 w;
	u32 h;
	u32 chunk_size;

	/// A 2D grid of chunks.
	vector<shared_ptr<WorldChunk>> world_grid;

	vector<shared_ptr<Drawable>> drawables;
	vector<shared_ptr<Dynamic>> dynamics;
};
