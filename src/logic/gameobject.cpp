
#include "logic/gameobject.h"

u32 GameObject::object_id = 0;

GameObject::GameObject( const Rectangle& rect )
: pos( rect.x, rect.y ), dim( rect.w, rect.h ), id( object_id++ ) {}

GameObject::GameObject( f32 x, f32 y, f32 w, f32 h )
: pos( x, y ), dim( w, h ), id( object_id++ ) {}

void GameObject::pretty_print( ostream& stream ) const {
	stream
		<< "GameObject(" << pos << ", "
		<< dim.x << "x" << dim.y << ")";
}

Rectangle GameObject::get_rect() const {
	return Rectangle( pos, dim );
}

CollisionFilter GameObject::collision_filter( GameObject& ) const {
	return CollisionFilter::Slide;
}

void GameObject::collision_callback( Collision& col, bool ) {
	using namespace Graphics;

	Renderer::main.draw_rect( get_rect(), ( col.data.overlap ? Colour::red : Colour::green ) * 0.1f, true );
}

void GameObject::death_callback( World& ) {
}

f32 GameObject::get_x() const {
	return pos.x;
}

f32 GameObject::get_y() const {
	return pos.y;
}

f32 GameObject::get_w() const {
	return dim.x;
}

f32 GameObject::get_h() const {
	return dim.y;
}

vec2 GameObject::get_pos() const {
	return pos;
}

GameObject& GameObject::set_pos( vec2 new_pos ) {
	pos = new_pos;
	return *this;
}

vec2 GameObject::get_dim() const {
	return dim;
}

vec2 GameObject::get_dimensions() const {
	return dim;
}
