
#pragma once

#include <fstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "prelude.h"

#include "utils/logger.h"
#include "physics/dynamic.h"
#include "entities/drawable.h"

#include "graphics/font.h"
#include "graphics/colour.h"
#include "graphics/window.h"
#include "graphics/renderer.h"
#include "graphics/textureatlas.h"

class Player;
#include "entities/player.h"
#include "entities/zombie.h"
#include "entities/bullet.h"
#include "entities/healthpack.h"

#include "math/vec2.h"

/// Quite self-explanatory.
enum class GameState {
	StatsScreen,
	GameOver,
	InGame,
	Menu,
};

/// An instance of the Gamestuff Engine.
/// Serves as the root of a game's hierarchy,
/// providing resource management for child components.
class Engine {
public:
	static Engine& get_instance();

	static constexpr auto SAVE_PATH = "./zombease-save.txt";
	static constexpr u8 MAX_STATS_ENTRIES = 5;

	static constexpr u32 window_w = 1200;
	static constexpr u32 window_h = 900;

	~Engine();

	/// Initialise the engine for actual use.
	/// Few things can be done directly in the ctor, because
	/// initialisation of SDL and other dependencies may fail,
	/// to which the game needs to respond appropriately.
	/// @return Whether the initialisation process was successful.
	bool cold init() important;

	/// Update the game and render a frame, from start to finish.
	/// This function should be called in a loop until it returns `false`.
	/// @return `false` in case the game should quit, `true` otherwise.
	bool hot frame() important;

	/// Get a reference to the World instance.
	World& get_world();

	/// Check whether a key is being held down.
	bool held( SDL_Keycode ) const;

	/// Getter for the TextureAtlas.
	shared_ptr<TextureAtlas> get_atlas();
	/// Getter for players.
	vector<shared_ptr<Player>> get_players();

	void load_stats();
	void save_stats() const;

private:
	static Engine instance;
	/// Instantiate the engine.
	/// @param world_width The width of the world, in pixels
	/// @param world_height The height of the world, in pixels
	Engine( u32 world_width, u32 world_height );

	/// Gameplay statistics.
	/// Pairs of (zombies killed, time survived)
	vector<pair<u32, i64>> stats;

	/// The time (in milliseconds) when the game started
	/// (user clicked Play in the menu).
	i64 game_started = 0;

	vector<shared_ptr<Drawable>> overlay_elements;

	GameState game_state = GameState::Menu;

	/// The Window object in use.
	Graphics::Window window;
	/// The title of the game window.
	const string window_title;

	/// The Texture used for displaying remaining lives.
	Graphics::Texture heart_texture;

	/// The font used for rendering text.
	Graphics::Font font;

	/// Provides texture resource management.
	shared_ptr<TextureAtlas> atlas;

	/// @debug remove this. seriously.
	shared_ptr<Player> player;

	/// A map of keys which are currently being held down.
	map<SDL_Keycode, bool> held_keys;
	/// A map of (mouse) buttons which are currently being held down.
	map<u8, bool> held_buttons;
	/// The game world.
	World world;

	/// Whether to display a GUI overlay for debugging purposes.
	bool debug_highlights =
	#ifdef CFG_DEBUG
		true;    // Start the game with debug_highlights on in debug configuration
	#else
		false;
	#endif

	/// Camera offset.
	vec2 camera = vec2::zero;

	/// The time of the previous frame.
	i64 last_time;

	void print_text( vec2 pos, const string& str, Graphics::Colour colour = Graphics::Colour::black );
	bool handle_event() important;
};
