
#pragma once

#include "prelude.h"

class Dynamic;
#include "physics/dynamic.h"
#include "entities/drawable.h"

/// A single chunk within the game world.
/// See World for more information.
class WorldChunk {
public:
	const u32 x;
	const u32 y;
	const u32 size;

	WorldChunk( u32 _x, u32 _y, u32 _size );

	/// Remove a Drawable from this chunk.
	/// @return Whether the removal was successful (object found and removed).
	bool remove( shared_ptr<Drawable> obj ) important;

	/// Remove a Dynamic from this chunk.
	/// @return Whether the removal was successful (object found and removed).
	bool remove( shared_ptr<Dynamic> obj ) important;

	/// Add a Drawable to this chunk.
	/// @return Whether the addition was successful.
	bool add( shared_ptr<Drawable> obj ) important;

	/// Add a Dynamic to this chunk.
	/// @return Whether the addition was successful.
	bool add( shared_ptr<Dynamic> obj ) important;

	vector<shared_ptr<Drawable>>& get_drawables();
	vector<shared_ptr<Dynamic>>& get_dynamics();

private:
	vector<shared_ptr<Drawable>> drawables;
	vector<shared_ptr<Dynamic>> dynamics;
};
