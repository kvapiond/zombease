
#include "logic/worldchunk.h"

WorldChunk::WorldChunk( u32 _x, u32 _y, u32 _size )
: x( _x ), y( _y ), size( _size ) {}

bool WorldChunk::remove( shared_ptr<Drawable> obj ) {
	let mut success = false;

	for ( u32 i = drawables.size() - 1; i < drawables.size(); i-- ) {
		if ( drawables[ i ] == obj ) {
			drawables.erase( drawables.begin() + (i32)i );
			success = true;
		}
	}

	return success;
}

bool WorldChunk::remove( shared_ptr<Dynamic> obj ) {
	let mut success = false;

	for ( u32 i = dynamics.size() - 1; i < dynamics.size(); i-- ) {
		if ( dynamics[ i ] == obj ) {
			dynamics.erase( dynamics.begin() + (i32)i );
			success = remove( static_cast<shared_ptr<Drawable>>( obj ) );
		}
	}

	return success;
}

bool WorldChunk::add( shared_ptr<Drawable> obj ) {
	drawables.push_back( obj );
	return true;
}

bool WorldChunk::add( shared_ptr<Dynamic> obj ) {
	dynamics.push_back( obj );
	return add( static_cast<shared_ptr<Drawable>>( obj ) );
}

vector<shared_ptr<Drawable>>& WorldChunk::get_drawables() {
	return drawables;
}

vector<shared_ptr<Dynamic>>& WorldChunk::get_dynamics() {
	return dynamics;
}
