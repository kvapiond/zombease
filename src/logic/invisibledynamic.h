
#pragma once

#include "prelude.h"
#include "physics/dynamic.h"
#include "graphics/renderer.h"

/// A Dynamic which doesn't render.
class InvisibleDynamic : public Dynamic {
public:
	InvisibleDynamic( const Rectangle& rect );
	virtual ~InvisibleDynamic() = default;

	bool draw( Graphics::Renderer& renderer ) const override;
	bool draw( Graphics::Renderer& renderer, vec2 _pos ) const override;
};
