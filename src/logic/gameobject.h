
#pragma once

#include "prelude.h"

#include "math/vec2.h"
#include "utils/pretty.h"
#include "math/rectangle.h"
#include "graphics/colour.h"
#include "graphics/renderer.h"

// This is ugly, but oh well.
/// @cyclic World <-> GameObject
class World;
class Collision;
enum class CollisionFilter;
#include "physics/collision.h"

/// A generic game object with a set position and size.
/// @todo docs!
class GameObject : public Pretty {
public:
	GameObject( const Rectangle& rect );
	GameObject( f32 x, f32 y, f32 _w, f32 _h );
	virtual ~GameObject() = default;

	/// Print the object's properties in a human-readable format.
	virtual void pretty_print( ostream& stream ) const;

	/// Get a Rectangle equivalent to the bounding box for this object.
	virtual Rectangle get_rect() const important;

	/// Change the solidity of the object (see GameObject::solid()).
	bool solid( bool );

	/// Check whether this object should react to collisions with another object.
	virtual CollisionFilter collision_filter( GameObject& other ) const important;

	/// React to a collision.
	/// @param col The collision that just happened
	/// @param moving Whether this object was the one that was moving
	virtual void collision_callback( Collision& col, bool moving );

	/// This method is triggered when the object is about to be removed from the world.
	virtual void death_callback( World& world );

	/// Move to a new position in the game world, optionally also
	/// respecting collisions (depends on the implementation).
	virtual bool move_to( World& world, vec2 target ) = 0;

	/// Getter for the x coordinate.
	/// @deprecated Use GameObject::get_pos() instead.
	f32  deprecated get_x() const important;
	/// Getter for the y coordinate.
	/// @deprecated Use GameObject::get_pos() instead.
	f32  deprecated get_y() const important;

	/// Getter for width.
	f32 deprecated get_w() const important;
	/// Getter for height.
	f32 deprecated get_h() const important;

	/// Getter for x and y coordinates.
	vec2 get_pos() const important;
	/// Setter for the x and y coordinates.
	GameObject& set_pos( vec2 new_pos );

	/// Getter for the width and height.
	vec2 get_dim() const important;
	/// Alias to GameObject::get_dim().
	vec2 get_dimensions() const important;

protected:
	/// The counter for assigning IDs (see GameObject::id).
	static u32 object_id;
	/// Whether this object should participate in collision detection.
	bool is_solid = false;

	/// Position in the game world.
	vec2 pos;
	/// The object's width and height (dimensions).
	vec2 dim;

	/// Every GameObject has a unique ID (see GameObject::object_id).
	u32 id;
};
