
#include "logic/world.h"

World::World( u32 _w, u32 _h, u32 _chunk_size )
: w( _w ), h( _h ), chunk_size( _chunk_size ) {
	world_grid.reserve( ( h / chunk_size ) * ( w / chunk_size ) );

	for ( u32 y = 0; y < h / chunk_size; y++ ) {
		for ( u32 x = 0; x < w / chunk_size; x++ ) {
			world_grid.emplace_back( new WorldChunk( x, y, chunk_size ) );
		}
	}
}

bool World::remove( set<shared_ptr<Dynamic>> objs ) {
	/// @fixme Why the hell is this needed? Why can't I do set<shared_ptr<Drawable>> foo = objs;?
	set<shared_ptr<Drawable>> drawable_objs;
	for ( let obj : objs ) {
		drawable_objs.insert( obj );
	}

	for ( u32 i = dynamics.size() - 1; i < dynamics.size(); i-- ) {
		let iter = objs.find( dynamics[ i ] );

		if ( iter != objs.end() ) {
			for ( let chunk : get_overlapping_chunks( **iter ) ) {
				if ( !chunk->remove( *iter ) ) {
					Logger::log << ERROR << "Dynamic not found within an overlapping chunk";
				}
			}

			dynamics.erase( dynamics.begin() + (i32)i );
			objs.erase( iter );
		}

		if ( objs.empty() ) {
			break;
		}
	}

	// return remove( drawable_objs ) && objs.empty(); would suffice,
	// but I'm afraid the compiler would swap the order of arguments
	// and optimise the remove() call away
	if ( remove( drawable_objs ) ) {
		return objs.empty();
	}

	return false;
}

bool World::remove( set<shared_ptr<Drawable>> objs ) {
	for ( u32 i = drawables.size() - 1; i < drawables.size(); i-- ) {
		let iter = objs.find( drawables[ i ] );

		if ( iter != objs.end() ) {
			( *iter )->death_callback( *this );
			for ( let chunk : get_overlapping_chunks( **iter ) ) {
				if ( !chunk->remove( *iter ) ) {
					Logger::log << WARN << "Drawable not found within an overlapping chunk (possibly removed by previous calls)";
				}
			}

			drawables.erase( drawables.begin() + (i32)i );
			objs.erase( iter );
		}

		if ( objs.empty() ) {
			return true;
		}
	}

	return objs.empty();
}

bool World::remove( shared_ptr<Drawable> obj ) {
	for ( let chunk : get_overlapping_chunks( *obj ) ) {
		if ( !chunk->remove( obj ) ) {
			Logger::log << ERROR << "Drawable not found in overlapping chunk";
		}
	}

	for ( u32 i = 0; i < drawables.size(); i++ ) {
		if ( drawables[ i ] == obj ) {
			obj->death_callback( *this );
			drawables.erase( drawables.begin() + (i32)i );
			return true;
		}
	}

	return false;
}

bool World::remove( shared_ptr<Dynamic> obj ) {
	for ( let chunk : get_overlapping_chunks( *obj ) ) {
		if ( !chunk->remove( obj ) ) {
			Logger::log << ERROR << "Dynamic not found in overlapping chunk";
		}
	}

	for ( u32 i = 0; i < dynamics.size(); i++ ) {
		if ( dynamics[ i ] == obj ) {
			dynamics.erase( dynamics.begin() + (i32)i );
			return remove( static_cast<shared_ptr<Drawable>>( obj ) );
		}
	}

	return false;
}

bool World::add( shared_ptr<Drawable> obj ) {
	drawables.push_back( obj );

	for ( let chunk : get_overlapping_chunks( *obj ) ) {
		if ( !chunk->add( obj ) ) {
			Logger::log << ERROR << "Could not add Drawable to a chunk";
		}
	}

	return true;
}

bool World::add( shared_ptr<Dynamic> obj ) {
	dynamics.push_back( obj );

	for ( let chunk : get_overlapping_chunks( *obj ) ) {
		if ( !chunk->add( obj ) ) {
			Logger::log << ERROR << "Could not add Dynamic to a chunk";
		}
	}

	return add( static_cast<shared_ptr<Drawable>>( obj ) );
}

vector<shared_ptr<Dynamic>> World::get_nearby( shared_ptr<Dynamic> obj ) const {
	vector<shared_ptr<Dynamic>> results;
	vector<shared_ptr<WorldChunk>> chunks;

	for ( let chunk : get_overlapping_chunks( *obj ) ) {
		let neighbours = get_nearby_chunks( chunk, true );
		chunks.insert( chunks.end(), neighbours.begin(), neighbours.end() );
	}

	sort( chunks.begin(), chunks.end() );
	chunks.erase( unique( chunks.begin(), chunks.end() ), chunks.end() );

	for ( let chunk : chunks ) {
		results.insert(
			results.end(),
			chunk->get_dynamics().begin(),
			chunk->get_dynamics().end()
		);
	}

	// We don't expect a huge number of objects per chunk, so it's unnecessary to
	// convert to a set and then back to a vector
	sort( results.begin(), results.end() );
	results.erase( unique( results.begin(), results.end() ), results.end() );

	return results;
}

vector<shared_ptr<WorldChunk>> World::get_nearby_chunks( shared_ptr<WorldChunk> chunk, bool include_self ) const {
	vector<shared_ptr<WorldChunk>> results;

	static const i8 directions[ 3 ] = { -1, 1, 0 };

	for ( u8 i = 0; i <3 /* Kate */; i++ ) {
		for ( u8 j = 0; j < 3; j++ ) {
			if ( i + j != 4 || include_self ) {
				// Weird casting to suppress Clang's -Wsign-conversion
				let x = (u32)( (i32)chunk->x + directions[ i ] );
				let y = (u32)( (i32)chunk->y + directions[ j ] );

				// Only two checks are necessary, thanks to underflows
				if ( x < w / chunk_size && y < h / chunk_size ) {
					results.push_back( world_grid[ y * w / chunk_size + x ] );
				}
			}
		}
	}

	return results;
}

vector<shared_ptr<WorldChunk>> World::get_overlapping_chunks( const GameObject& obj ) const {
	vector<shared_ptr<WorldChunk>> results;

	let y_start = (i64)min( h, (u32)( obj.get_pos().y / (f32)chunk_size ) * chunk_size );
	let x_start = (i64)min( w, (u32)( obj.get_pos().x / (f32)chunk_size ) * chunk_size );

	let y_end = min( (i64)( obj.get_pos().y + (f32)obj.get_dim().y ), (i64)h );
	let x_end = min( (i64)( obj.get_pos().x + (f32)obj.get_dim().x ), (i64)w );

	for ( i64 y = y_start; y < y_end; y += chunk_size ) {
		for ( i64 x = x_start; x < x_end; x += chunk_size ) {
			let index = (u32)( y / (i64)chunk_size ) * w / chunk_size + (u32)( x / (i64)chunk_size );
			results.push_back( world_grid[ index ] );
		}
	}

	return results;
}

void World::render_debug_overlay() const {
	using namespace Graphics;
	Renderer::main.set_draw_blend_mode( SDL_BLENDMODE_BLEND );

	for ( u32 y = 0; y < h / chunk_size; y++ ) {
		for ( u32 x = 0; x < w / chunk_size; x++ ) {
			let rect = Rectangle( x * chunk_size, y * chunk_size, chunk_size, chunk_size );
			let alpha = (u8)( 10 * world_grid[ y * w / chunk_size + x ]->get_drawables().size() );
			let colour = Colour( 255, 0, 0, alpha );

			Renderer::main.draw_rect( rect, colour, true );
		}
	}
}

vector<shared_ptr<Drawable>>& World::get_drawables() {
	return drawables;
}

vector<shared_ptr<Dynamic>>& World::get_dynamics() {
	return dynamics;
}
