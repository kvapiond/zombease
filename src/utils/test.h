
#pragma once

#include "prelude.h"
#include "utils/logger.h"

let APPROX_EPSILON = 0.01f;

/// Manages the list of unit tests.
class Test {
public:
	static const u8 MIN_RECOMMENDED_CHECKS = 4;

	virtual ~Test() = 0;

	struct Result {
		const bool result;
		const u16 total;
		const u16 passed;

		Result( bool _res, u16 _total, u16 _passed );

		operator bool() const;
	};

	/// Run sanity tests.
	static bool run_tests() important;

private:
	static bool dirty;
	static vector<function<Result()>> list;
	static void prepare_tests();
};

/// Helper macro for test definition (header).
/// @param name The name of this test, without quotes
#define TEST_HEADER( name ) \
	Test::list.push_back(                                        \
	[] () -> Test::Result {                                      \
		Logger::log.scope( __FILE__ " - " QUOTE( name ) );       \
		let mut result = true;                                   \
		u16 total = 0;                                           \
		u16 passed = 0;                                          \
		do {} while( 0 )

/// Helper macro for test definition (footer).
#define TEST_FOOTER() \
	Logger::log << END_SCOPE;                     \
	return Test::Result( result, total, passed ); \
} );

#define assert_eq_helper( a, b ) do {     \
	total++;                              \
	passed++;                             \
	if ( !( ( a ) == ( b ) ) ) {          \
		result = false;                   \
		Logger::log << ERROR;             \
		passed--;                         \
	}                                     \
	Logger::log << ss.str();              \
} while ( 0 )

/// Pass a check if two values are equal.
#define assert_eq( a, b ) do {     \
	std::stringstream ss;          \
	ss                             \
		<< #a << " == " << #b      \
		<< "\t(" << a              \
		<< " == "                  \
		<< b << ")";               \
	assert_eq_helper( a, b );      \
} while ( 0 )

/// Pass a check if two values are less than #APPROX_EPSILON apart.
#define assert_approx( a, b ) do {                             \
	std::stringstream ss;                                      \
	ss                                                         \
		<< #a << " ≈ " << #b                                   \
		<< "\t(" << a                                          \
		<< " ≈ "                                               \
		<< b << ")";                                           \
	assert_eq_helper(                                          \
		abs( ( a ) - ( b ) ) < APPROX_EPSILON,                 \
		true                                                   \
	);                                                         \
} while ( 0 )
