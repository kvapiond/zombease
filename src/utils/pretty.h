
#pragma once

#include "prelude.h"

class Pretty {
public:
	/// Convert this object to a human-readable format.
	virtual void pretty_print( ostream& stream ) const = 0;

	friend ostream& operator<<( ostream& stream, const Pretty& pretty );
};

inline ostream& operator<<( ostream& stream, const Pretty& pretty ) {
	pretty.pretty_print( stream );
	return stream;
}
