
#include "utils/test.h"

Test::Result::Result( bool _res, u16 _total, u16 _passed )
: result( _res ), total( _total ), passed( _passed ) {}

Test::Result::operator bool() const {
	return result;
}

vector<function<Test::Result()>> Test::list;
bool Test::dirty = true;

bool Test::run_tests() {
	SCP();
	let mut success = true;
	u16 checks_passed = 0;
	u16 tests_passed = 0;
	u16 checks_total = 0;

	Logger::log << "Running tests in "
	#ifdef CFG_RELEASE
		"release"
	#elif CFG_DEBUG
		"debug"
	#else
		"unknown"
	#endif
	" configuration" << "";

	let start_time = time();

	if ( Test::dirty ) {
		Test::prepare_tests();

		Test::dirty = false;
	}

	let tests = Test::list;
	for ( let test : tests ) {
		let result = test();
		checks_total += result.total;
		checks_passed += result.passed;

		if ( result.total < Test::MIN_RECOMMENDED_CHECKS ) {
			Logger::log << WARN << "Test has very few checks, consider expanding it";
		}

		let appender = Logger::line( false );

		if ( result ) {
			tests_passed++;
			appender << Logger::GREEN_TICK << " Passed (" << result.total << " checks)";
		} else {
			success = false;
			appender << ERROR << Logger::RED_CROSS << " Failed"
			         << " (" << result.passed << "/" << result.total << " checks successful)";
		}
	}

	// Summarize test results on a single line
	( Logger::log << "" ).line( false )
		<< ( success ? INFO : ERROR )
		<< ( success ? Logger::GREEN_TICK : Logger::RED_CROSS ) << " "
		<< checks_passed << "/" << checks_total
		<< " (" << 100.0f * (f32)checks_passed / (f32)checks_total << "%)"
		<< " checks passed in "
		<< tests_passed << "/" << tests.size() << " tests"
		<< " (" << 100.0f * (f32)tests_passed / (f32)tests.size() << "%)"
		" in ~" << time( -start_time ) << "ms";

	// Start a new line in the log, in green
	let mut appender = Logger::line( false ) << "  \033[32m";

	for ( u16 i = 0; i < checks_total; i++ ) {
		// Once we reach the passed/failed boundary, switch to red
		if ( i == checks_passed ) { appender << "\033[31m"; }
		appender << Logger::FULL_CIRCLE;
	}

	// Finish the line by resetting the colour
	appender << "\033[0m";

	Logger::log << END_SCOPE;
	return success;
}
