
#pragma once

#include "prelude.h"

class LoadableResource {
public:
	inline LoadableResource( const string& path ) {
		load( path );
	}

protected:
	LoadableResource load( const string& path ) = 0;
}
