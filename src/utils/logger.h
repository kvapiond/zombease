
/// @file logger.h
/// Definition of the Logger class and associated entities.

#pragma once

#include <iterator>
#include <sstream>
#include <iomanip>
#include <time.h>

using std::istream_iterator;

#include "prelude.h"

/// Start a new logger scope.
#define SCP()	Logger::log.scope( string( Logger::FUNCTION_SCOPE_PREFIX ) + string( __PRETTY_FUNCTION__ ) )

/// Start a new logger scope and list function arguments.
/// @param _args The parameters passed to the function being called
#define SCOPE( _args... )	do {                                                     \
	Logger::log.scope(                                                               \
		string( Logger::FUNCTION_SCOPE_PREFIX ) + string( __PRETTY_FUNCTION__ ),     \
		false                                                                        \
	);                                                                               \
	std::string _s = #_args;                                                         \
	std::replace( _s.begin(), _s.end(), ',', ' ' );                                  \
	std::stringstream _ss( _s );                                                     \
	std::istream_iterator<std::string> _it( _ss );                                   \
	Logger::log << START_PARAM_LIST;                                                 \
	report( Logger::log, _it, _args );                                               \
	Logger::log << END_SCOPE_DEF;                                                    \
} while ( 0 )

/// Display the value of an expression.
/// Intended for use with Logger::LineAppender as such:
/// ```
/// string foo = "bar";
/// line_appender << VAR( foo );
/// // outputs "[<time>]	foo = bar"
/// ```
/// but will work with anything that accepts arbitrary types
/// via `operator<<`.
/// @param x The variable or expression to display.
#define VAR( x ) #x " =" << x

/// Control the flow of the log (use is mostly internal).
/// The useful values here are DEBUG, INFO, WARN, and ERROR - the
/// different importance settings for a log entry. It'd be nice
/// if this could be an `enum class` instead, but that would
/// mean one would unfortunately have to write out the name of
/// the `enum class` before using it, followed by a double colon.
/// That's simply too much code.
enum LOGGER_CONTROL {
	// logger directives
	END_SCOPE = 0, START_PARAM_LIST = 1, END_PARAM_LIST = 2, START_SCOPE_DEF = 3, END_SCOPE_DEF = 4,
	// message types
	DEBUG = 35, INFO = 37, WARN = 93, ERROR = 91
};

/// Provides an interface for logging various debugging data.
/// The Logger is quite advanced. It organizes logging output
/// by "scopes," which have to be started with #SCP() or #SCOPE()
/// and ended with `Logger::log << END_SCOPE`. These show the
/// flow of the program at runtime. Here is an example of the
/// produced output:
/// ![](../../src/docs/img/log-screenshot.png)
/// To offer such advanced information, it utilises the GCC
/// macro `__PRETTY_FUNCTION__`. To list function arguments
/// along with their values (like in the
/// TextureAtlas::load_texture() call in the picture), the user
/// has to pass them all to #SCOPE(). Arbitrary scopes can be
/// also created with `Logger::log.scope()`.
/// While multiple instances are allowed, the static Logger
/// instance is meant to be invoked most of the time.
class Logger {
public:
	static constexpr auto FUNCTION_SCOPE_PREFIX = "@ ";
	static const string FULL_CIRCLE;
	static const string GREEN_TICK;
	static const string RED_CROSS;

	/// An object for writing to a single line of a Logger.
	/// `log << foo` writes to a new line and returns `log`. However,
	/// it is often desired to output multiple (shorter) values to
	/// a single line. LineAppender does just that -- it's a proxy
	/// (to a Logger) which doesn't insert a newline with each
	/// `operator<<` invocation. It inserts a space instead,
	/// and optionally disables any splits altogether.
	class LineAppender {
	public:
		LineAppender( Logger&, bool _split = true );
		~LineAppender() = default;

		LineAppender( const LineAppender& ) = default;
		LineAppender( LineAppender&& ) = default;

		friend LineAppender operator<<( LineAppender, const string& );
		friend LineAppender operator<<( LineAppender, LOGGER_CONTROL );

		template<typename T>
		friend LineAppender operator<<( LineAppender, const T& );

	private:
		// Just two bools and a pointer make up a LineAppender.
		// That's why it's fine to use the default move and copy ctors.
		bool first = true;
		Logger& log;
		bool split;

		ostream& get_output() const;
		bool get_flush() const;
		void set_flush( bool v );
	};

	friend class LineAppender;

	static Logger log;

	Logger( ostream& stream, bool _colour = true );

	i64 get_time();
	Logger& scope( const string& name = "", bool short_def = true );

	static LineAppender line( bool split = true ) {
		return Logger::log.get_line_appender( split );
	}

	LineAppender get_line_appender( bool split = true );
	LineAppender operator()( bool split = true );

	friend Logger& operator<<( Logger&, const string& );
	friend Logger& operator<<( Logger&, LOGGER_CONTROL );

	template<typename T>
	friend Logger& operator<<( Logger&, const T& value );

private:
	u16 n_scope;
	const bool colour;

	i64 init_time;
	ostream& output;

	string log_prefix;

	bool flush = true;
	bool param_name = true;
	bool single_line = false;
	bool process_params = false;
	bool entering_scope = false;

	LOGGER_CONTROL msg_type = INFO;

	void end_scope();
	void start_param_list();
	void end_param_list();
	void start_scope_def();
	void end_scope_def();
};

inline void report( Logger& log, istream_iterator<string> ) {
	log << END_PARAM_LIST;
}

template<typename T, typename... Args>
inline void report( Logger& log, istream_iterator<string> it, T value, Args... args ) {
	ostringstream ss;
	ss << value;

	log << *it << ss.str();
	report( log, ++it, args... );
}

template<typename T>
Logger& operator<<( Logger& log, const T& value ) {
	stringstream ss;
	ss << value;

	return log << ss.str();
}

template<typename T>
Logger::LineAppender operator<<( Logger::LineAppender app, const T& value ) {
	stringstream ss;
	ss << value;

	return app << ss.str();
}
