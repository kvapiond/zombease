
/// @file logger.cpp
/// Implementation of the Logger class.

#include "utils/logger.h"

Logger Logger::log = Logger( cout );
const string Logger::FULL_CIRCLE = "●";
const string Logger::GREEN_TICK = "\033[32m✓\033[0m";
const string Logger::RED_CROSS = "\033[31m✘\033[0m";

Logger::LineAppender::LineAppender( Logger& _log, bool _split )
: log( _log ), split( _split ) {}

ostream& Logger::LineAppender::get_output() const {
	return log.output;
}

bool Logger::LineAppender::get_flush() const {
	return log.flush;
}

void Logger::LineAppender::set_flush( bool v ) {
	log.flush = v;
}

Logger::LineAppender operator<<( Logger::LineAppender app, const string& value ) {
	if ( app.first ) {
		let flush = app.get_flush();
		app.set_flush( false );
		app.log << value;
		app.set_flush( flush );

		app.first = false;
		return app;
	}

	if ( app.split ) {
		app.get_output() << " " << value;
	} else {
		app.get_output() << value;
	}

	return app;
}

Logger::LineAppender operator<<( Logger::LineAppender app, LOGGER_CONTROL v ) {
	if ( app.first || v == END_SCOPE ) {
		app.log << v;
	}

	return app;
}

Logger::Logger( ostream& stream, bool _colour )
: n_scope( 0 ), colour( _colour ), output( stream )  {
	init_time = time();
}

i64 Logger::get_time() {
	return time( -init_time );
}

string coloured( u32 c, const string& text ) {
	ostringstream ss;
	ss << "\033[3" << ( c % 7 + 1 ) << "m" << text << "\033[0m";
	return ss.str();
}

Logger& Logger::scope( const string& name, bool short_def ) {
	n_scope++;

	if ( colour ) {
		( *this ) << coloured( n_scope, "\\ " + name );
	} else {
		( *this ) << ( "\\ " + name );
	}

	if ( colour ) {
		log_prefix += coloured( n_scope, " |" );
	} else {
		log_prefix += " |";
	}

	if ( !short_def ) {
		( *this ) << START_SCOPE_DEF;
	}

	return *this;
}

Logger::LineAppender Logger::get_line_appender( bool split ) {
	if ( single_line ) {
		output << endl;
	} 

	single_line = true;

	return Logger::LineAppender( *this, split );
}

void Logger::end_scope() {
	/// @fixme why 2?
	u16 offset = 2;

	if ( n_scope <= 0 ) {
		*this << WARN << "Received END_SCOPE at root level";
		return;
	}

	if ( entering_scope ) {
		*this << WARN << "Received END_SCOPE inside scope definition";
	}

	if ( colour ) {
		/// @fixme magic number! The length of the prefix portion (together with ANSI codes)
		offset = 11;
	}

	log_prefix = log_prefix.substr( 0, log.log_prefix.length() - offset );
	*this << "/";

	n_scope--;
}

void Logger::start_param_list() {
	if ( !entering_scope ) {
		*this << WARN << "Received START_PARAM_LIST outside of a scope definition";
	}

	using std::setw;
	using std::setfill;

	output
		<< "["
		<< setw( 8 )
		<< setfill( ' ' )
		<< log.get_time() << "]\t"
		<< log.log_prefix
		<< "> Arguments: ";

	process_params = true;
}

void Logger::end_param_list() {
	output << endl;
	process_params = false;
}

void Logger::start_scope_def() {
	if ( entering_scope ) {
		*this << WARN << "Received START_SCOPE_DEF inside a scope definition";
	}

	entering_scope = true;
}

void Logger::end_scope_def() {
	if ( !entering_scope ) {
		*this << WARN << "Received END_SCOPE_DEF without a definition to end";
	}

	entering_scope = false;
	*this << "";
}

Logger::LineAppender Logger::operator()( bool split ) {
	return get_line_appender( split );
}

/// @todo document this
Logger& operator<<( Logger& log, LOGGER_CONTROL v ) {
	switch ( v ) {
		case END_SCOPE:
			log.end_scope();
			break;

		case START_PARAM_LIST:
			log.start_param_list();
			break;

		case END_PARAM_LIST:
			log.end_param_list();
			break;

		case START_SCOPE_DEF:
			log.start_scope_def();
			break;

		case END_SCOPE_DEF:
			log.end_scope_def();
			break;

		default:
			log.msg_type = v;
			break;
	}

	return log;
}

Logger& operator<<( Logger& log, const string& str ) {
	if ( !log.entering_scope ) {
		string end_colour = "]\t";

		if ( log.colour ) {
			log.output << "\033[" << ( log.msg_type != INFO ? "1;" : "" ) << log.msg_type << "m";
			end_colour += "\033[0m";
		}

		using std::setw;
		using std::setfill;

		if ( log.single_line && log.flush ) {
			log.output << endl;
			log.single_line = false;
		}

		log.output
			<< "["
			<< setw( 8 )
			<< setfill( ' ' )
			<< log.get_time() << end_colour
			<< log.log_prefix
			<< str;

		if ( log.flush ) {
			log.output << endl;
		}
	} else {
		if ( log.process_params ) {
			if ( log.param_name ) {
				log.output << str << " = ";
			} else {
				log.output << str << "; ";
			}

			log.param_name = !log.param_name;
		} else if ( log.flush ) {
			log.output << str << endl;
		}
	}

	log.msg_type = INFO;

	return log;
}
