
#pragma once

#include "utils/test.h"

#include "math/vec2.h"
#include "math/geometry.h"

TEST_HEADER( vectors );

using namespace Geometry;

Logger::log.scope( "normalise() checks" );

let mut v = vec2( 12.34f, -9.75f );

let mut n = v.normalise();
assert_approx( n.x,  0.784638564f );
assert_approx( n.y, -0.619953485f );
assert_approx( n.magnitude(), 1.0f );

v.x = 0.0f;
v.y = 0.0f;

n = v.normalise();
assert_eq( n, vec2::zero );
assert_eq( n.magnitude(), 0.0f );

v.x = 0.1f;
v.y = 0.1f;

n = v.normalise();
assert_eq( n, vec2( 0.707106781f, 0.707106781f ) );
assert_eq( n.magnitude(), 1.0f );

Logger::log << END_SCOPE;



Logger::log.scope( "dot() checks" );

let mut a = vec2( 1.0f, 1.0f );
let mut b = vec2( 6.0f, 7.0f );
let mut d = a.dot( b );

assert_eq( a.dot( b ), b.dot( a ) );
assert_eq( d, 13.0f );

a = vec2( -3.22f, 0.1f );
b = vec2( 23.0f, 50.0f );
d = a.dot( b );

assert_eq( a.dot( b ), b.dot( a ) );
assert_eq( d, -69.06f );

a = vec2( -1.0f, 1.0f );
b = vec2(  1.0f, 1.0f );
d = a.dot( b );

assert_eq( d, 0.0f );

Logger::log << END_SCOPE;



Logger::log.scope( "cross() checks" );

a = vec2(  3.21f,  1.23f );
b = vec2( -2.46f, -3.69f );

assert_eq( a.cross( b ), -8.8191f );
assert_eq( b.cross( a ),  8.8191f );

a = vec2( -5.713f, 73.19f );
b = vec2(  0.1f,    4.39f );

assert_eq( a.cross( b ), -32.39907f );
assert_eq( b.cross( a ),  32.39907f );

Logger::log << END_SCOPE;

TEST_FOOTER();
