
#pragma once

#include "utils/test.h"
#include "math/geometry.h"

TEST_HEADER( intersections );

using namespace Geometry;
Logger::log.scope( "get_lines_intersection_fraction() checks" );

let frac1 = get_lines_intersection_fraction(
	vec2( -1.0f, 0.0f ), vec2( 1.0f,  0.0f ),
	vec2(  0.0f, 1.0f ), vec2( 0.0f, -1.0f )
);

assert_eq( frac1, 0.5f );

let frac2 = get_lines_intersection_fraction(
	vec2( 0.0f, 1.0f ), vec2( 1.0f, 1.0f ),
	vec2( 0.0f, 0.0f ), vec2( 1.0f, 0.0f )
);

assert_eq( frac2, 1.0f / 0.0f );

let frac3 = get_lines_intersection_fraction(
	vec2( 0.0f, -1.0f ), vec2( 2.0f,  1.0f ),
	vec2( 1.0f,  1.0f ), vec2( 2.0f, -1.0f )
);

let x3 = frac3 * 2.0f;
let y3 = -1.0f + frac3 * 2.0f;

assert_eq( x3, 1.0f + 1.0f / 3.0f );
assert_approx( y3, 1.0f / 3.0f );

Logger::log << END_SCOPE;

TEST_FOOTER();
