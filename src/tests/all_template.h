
/// @file all_template.h
/// The template used for generating `tests/all.h`.
/// The `make` recipe for `all.h` collects test headers
/// from the `tests/` directory and injects them as
/// includes into this template. This means a single
/// `#include` directive can be used in the main file
/// and prevents manual management of the test list.

/// @file tests/all.h
/// This file has been automagically generated from
/// `src/tests/all_template.h` and other test files
/// in the `src/tests/` directory. See the template
/// file for details.

#pragma once

#include "utils/test.h"

void Test::prepare_tests() {
	// The following line will be substituted automagically in tests/all.h
	<TEST_INCLUDES>

	Test::dirty = false;
}
