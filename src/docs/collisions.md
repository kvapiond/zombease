
# Collision Mechanics

Collisions are handled by the Dynamic class and/or its subclasses.

For each Dynamic processed, we
-# Gather all Dynamic objects it could possibly collide with
	- this is done by querying the chunks close to the ones we intersect
-# Find all objects it is currently intersecting
-# Attempt to resolve the ongoing intersections
-# Find all objects that it will collide with or pass through in the next frame
-# Attempt to satisfy the future collisions

Since all objects are handled as axis-aligned bounding boxes, these steps
are performed with the help of the Rectangle struct, especially Rectangle::minkowski().
Minkowski difference is used to detect and fix ongoing collisions, and then perform swept
collision detection for collisions about to happen in the next frame. This approach
prevents tunneling (objects passing through other objects when they travel too fast
in a single frame).

Zombease's collision mechanics were implemented based on the ideas presented
[here](https://hamaluik.com/posts/swept-aabb-collision-using-minkowski-difference/).
Many thanks to Kenton Hamaluik for putting together such a nice tutorial!

![zombie prop](../src/assets/zmb01.bmp)
