
/// @file main.cpp
/// Execution entry point.
/// @author Andrew Kvapil <viluon@espiv.net> <kvapiond@fit.cvut.cz>

#include "prelude.h"

#include "utils/logger.h"
#include "logic/engine.h"

#include "tests/all.h"

let TEST_ONLY = string( "--test-only" );

int main( int n_args, const char** raw_args ) {
	let mut args = map<string, bool>();

	// Process command line arguments
	for ( u16 i = 0; i < (u16)n_args; i++ ) {
		args.insert( make_pair( string( raw_args[ i ] ), true ) );
	}

	if (
		args[ TEST_ONLY ]
		#ifdef CFG_DEBUG
			// Always run tests in debug configuration
			|| true
		#endif
	) {
		let test_result = Test::run_tests();

		if ( args[ TEST_ONLY ] ) {
			return !test_result;
		}
	}

	// Instantiate the engine
	let mut gamestuff = Engine::get_instance();

	// Initialize it
	if ( !gamestuff.init() ) {
		return 1;
	}

	// Run the game
	Logger::log << DEBUG << "Starting main game loop";
	while ( gamestuff.frame() );

	gamestuff.save_stats();

	return 0;
}
