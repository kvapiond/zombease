
#include "entities/bullet.h"

Bullet::Bullet( f32 dmg, vec2 _pos, vec2 _velocity, shared_ptr<GameObject> _owner, shared_ptr<Spritesheet> _spritesheet )
: Dynamic( _pos, _spritesheet ), damage( dmg ), velocity( _velocity ), owner( _owner ) {}

bool Bullet::alive() const {
	return !hit;
}

bool Bullet::update( Engine& engine, i64 dt ) {
	move_to( engine.get_world(), pos + velocity * (f32)dt );
	return !hit;
}

CollisionFilter Bullet::collision_filter( GameObject& other ) const {
	if ( typeid( other ) == typeid( Zombie& ) ) {
		return CollisionFilter::Bounce;
	}

	return CollisionFilter::Ignore;
}

void Bullet::collision_callback( Collision& col, bool actor ) {
	Logger::log << *col.data.other;
	if ( actor ) {
		Logger::log << "hitting it";
		dynamic_cast<Pawn*>( &*col.data.other )->hurt( damage );
		hit = true;
	}
}
