
#pragma once

#include "prelude.h"

#include "logic/world.h"
#include "physics/dynamic.h"
#include "graphics/renderer.h"
#include "graphics/spritesheet.h"
#include "entities/inventoryitem.h"

/// A stateful Dynamic which renders different animations in different states.
class Pawn : public Dynamic {
public:
	Pawn( f32 hp, vec2 _pos, shared_ptr<Spritesheet> _spritesheet );
	virtual ~Pawn() = default;

	bool move_to( World& world, vec2 target ) override;
	bool draw( Graphics::Renderer& renderer, vec2 _pos ) const override;

	bool alive() const override important;

	f32 get_health() const important;
	void set_health( f32 hp );

	void pretty_print( ostream& stream ) const override;

	GameObject& hurt( f32 damage );

protected:
	shared_ptr<InventoryItem> equipped_item = nullptr;
	f32 health;
};
