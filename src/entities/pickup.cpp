
#include "entities/pickup.h"

Pickup::Pickup( i64 duration, vec2 _pos, shared_ptr<Spritesheet> _spritesheet )
: Dynamic( _pos, _spritesheet ), expiration_time( time() + duration ) {}

CollisionFilter Pickup::collision_filter( GameObject& other ) const {
	if ( typeid( other ) == typeid( Player& ) ) {
		return CollisionFilter::Slide;
	}

	return CollisionFilter::Ignore;
}

bool Pickup::alive() const {
	return time() <= expiration_time;
}
