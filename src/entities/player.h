
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"
#include "entities/pawn.h"
#include "physics/dynamic.h"
#include "physics/collision.h"
#include "graphics/spritesheet.h"

class Engine;
#include "logic/engine.h"

/// The player-controlled Pawn.
class Player : public Pawn {
public:
	Player( f32 hp, vec2 _pos, shared_ptr<Spritesheet> _spritesheet );
	~Player() = default;

	void pretty_print( ostream& stream ) const override;

	bool move_to( World& world, vec2 target ) override;

	void collision_callback( Collision& col, bool moving ) override;

	// Dynamic is an abstract class
	bool update( Engine&, i64 ) override;

	// Drawable defaults are enough
};
