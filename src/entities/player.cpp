
#include "entities/player.h"

Player::Player( f32 hp, vec2 _pos, shared_ptr<Spritesheet> _spritesheet )
: Pawn( hp, _pos, _spritesheet ) {}

void Player::pretty_print( ostream& stream ) const {
	stream << "Player (";
	Pawn::pretty_print( stream );
	stream << ")";
}

bool Player::move_to( World& world, vec2 target ) {
	/// @todo react by changing the spritesheet
	let result = Pawn::move_to( world, target );

	return result;
}

void Player::collision_callback( Collision& col, bool actor ) {
	if ( typeid( *col.data.other ) == typeid( Zombie ) ) {
		let zmb = dynamic_cast<Zombie*>( &*col.data.other );

		if ( zmb != nullptr ) {
			swap( col.data.object, col.data.other );
			zmb->collision_callback( col, !actor );
			swap( col.data.object, col.data.other );
		}

	} else if ( typeid( *col.data.other ) == typeid( HealthPack ) ) {
		let hp = dynamic_cast<HealthPack*>( &*col.data.other );

		if ( hp != nullptr ) {
			swap( col.data.object, col.data.other );
			hp->collision_callback( col, !actor );
			swap( col.data.object, col.data.other );
		}
	}
}

bool Player::update( Engine& engine, i64 delta_time ) {
	let speed = 3;

	let mut target = pos;

	f32 distance = (f32)delta_time * speed / 10.0f;
	if ( engine.held( SDLK_w ) || engine.held( SDLK_UP    ) ) {
		target.y -= distance;
	}

	if ( engine.held( SDLK_s ) || engine.held( SDLK_DOWN  ) ) {
		target.y += distance;
	}

	if ( engine.held( SDLK_a ) || engine.held( SDLK_LEFT  ) ) {
		target.x -= distance;
	}

	if ( engine.held( SDLK_d ) || engine.held( SDLK_RIGHT ) ) {
		target.x += distance;
	}

	if ( target != pos ) {
		move_to( engine.get_world(), target );
	}

	return true;
}
