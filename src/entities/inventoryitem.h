
#pragma once

#include "prelude.h"
#include "entities/drawable.h"

/// An item inside a Pawn's inventory.
class InventoryItem : public Drawable {
public:
	InventoryItem() = default;
	virtual ~InventoryItem() = default;
};
