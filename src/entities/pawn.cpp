
#include "entities/pawn.h"

Pawn::Pawn( f32 hp, vec2 _pos, shared_ptr<Spritesheet> _spritesheet )
: Dynamic( _pos, _spritesheet ), health( hp ) {}

bool Pawn::move_to( World& world, vec2 target ) {
	/// @todo react by changing the spritesheet
	return Dynamic::move_to( world, target );
}

bool Pawn::draw( Graphics::Renderer& renderer, vec2 _pos ) const {
	Drawable::draw( renderer, _pos );

	if ( equipped_item != nullptr ) {
		equipped_item->draw( renderer, _pos );
	}

	return true;
}

bool Pawn::alive() const {
	return health > 0.0f;
}

void Pawn::pretty_print( ostream& stream ) const {
	stream << "Pawn(";
	Dynamic::pretty_print( stream );
	stream << ")";
}

f32 Pawn::get_health() const {
	return health;
}

void Pawn::set_health( f32 hp ) {
	health = hp;
}

GameObject& Pawn::hurt( f32 damage ) {
	health -= damage;
	return *this;
}
