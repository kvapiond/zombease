
#pragma once

#include "prelude.h"
#include "physics/dynamic.h"
#include "physics/collision.h"
#include "graphics/spritesheet.h"
class Player;

/// A consumable item that can be picked up from the ground.
class Pickup : public Dynamic {
public:
	Pickup( i64 duration, vec2 _pos, shared_ptr<Spritesheet> _spritesheet );

	CollisionFilter collision_filter( GameObject& other ) const override;

	inline bool update( Engine&, i64 ) override {
		return alive();
	}

	bool alive() const override;

protected:
	i64 expiration_time;
};

#include "entities/player.h"
