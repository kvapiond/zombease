
#pragma once

#include "prelude.h"

#include "math/vec2.h"
#include "entities/pawn.h"
#include "physics/dynamic.h"
#include "physics/collision.h"

class Engine;
#include "logic/engine.h"

/// The thing you shoot from a gun.
class Bullet : public Dynamic {
public:
	Bullet( f32 dmg, vec2 _pos, vec2 _velocity, shared_ptr<GameObject> _owner, shared_ptr<Spritesheet> _spritesheet );
	virtual ~Bullet() = default;

	bool alive() const override;
	CollisionFilter collision_filter( GameObject& other ) const override;
	void collision_callback( Collision& col, bool moving ) override;
	bool update( Engine& engine, i64 dt ) override;

private:
	f32 damage;
	vec2 velocity;
	shared_ptr<GameObject> owner;

	bool hit = false;
};
