
#include "entities/healthpack.h"

Graphics::Texture HealthPack::heart( nullptr );

HealthPack::HealthPack( f32 hp, vec2 _pos )
: Pickup( 10000, _pos, shared_ptr<Spritesheet>( new Spritesheet( "healthpack", heart ) ) ), value( hp ) {}

void HealthPack::collision_callback( Collision& col, bool ) {
	let pawn = dynamic_cast<Pawn*>( &*col.data.other );
	if ( pawn != nullptr ) {
		pawn->hurt( -value );
		expiration_time = -1;
	}
}
