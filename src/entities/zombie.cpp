
#include "entities/zombie.h"

u32 Zombie::kill_count = 0;

Zombie::Zombie( f32 hp, vec2 _pos, shared_ptr<Spritesheet> _spritesheet )
: Pawn( hp, _pos, _spritesheet ) {}

Zombie::~Zombie() {
	kill_count++;
}

bool Zombie::update( Engine& engine, i64 dt ) {
	let player = *engine.get_players().begin();
	let distance = player->get_pos() + player->get_dimensions() / 2 - pos - get_dimensions() / 2;

	if ( distance.magnitude() > 300.0f || true ) {
		let new_pos = pos + distance.normal() * ( (f32)dt / 10.0f );

		move_to( engine.get_world(), new_pos );
	}

	return alive();
}

void Zombie::collision_callback( Collision& col, bool ) {
	if ( typeid( *col.data.other ) == typeid( Player ) && cooldown < time() ) {
		Logger::log << DEBUG << "attack!";
		dynamic_cast<Pawn*>( &*col.data.other )->hurt( 2.0f );
		cooldown = time() + ATTACK_COOLDOWN;
	}
}

void Zombie::death_callback( World& world ) {
	if ( rand() % 3 == 0 ) {
		if ( !world.add( shared_ptr<Dynamic>(
			new HealthPack( 4.0f, pos )
		) ) ) {
			Logger::log << ERROR << "Failed to place HealthPack";
		}
	}
}
