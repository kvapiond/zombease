
#pragma once

#include "prelude.h"
#include "math/rectangle.h"

#include "logic/gameobject.h"
#include "graphics/renderer.h"
#include "graphics/spritesheet.h"

/// A GameObject that can be drawn to the screen.
class Drawable : public GameObject {
public:
	Drawable( const Rectangle& rect );
	Drawable( f32, f32, shared_ptr<Spritesheet> );
	virtual ~Drawable() = default;

	void pretty_print( ostream& stream ) const override;

	/// Render this Drawable.
	virtual bool draw( Graphics::Renderer& renderer ) const;
	/// Render this Drawable at the provided coordinates.
	virtual bool draw( Graphics::Renderer& renderer, vec2 _pos ) const;

protected:
	shared_ptr<Spritesheet> spritesheet;
};
