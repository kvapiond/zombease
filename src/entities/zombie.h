
#pragma once

#include "prelude.h"

#include "logic/engine.h"
#include "entities/pawn.h"
#include "physics/collision.h"
#include "entities/healthpack.h"

/// A hostile Pawn which chases the Player.
class Zombie : public Pawn {
public:
	/// Zombie attack cooldown, in ms.
	static constexpr i64 ATTACK_COOLDOWN = 1500;
	static u32 kill_count;

	Zombie( f32 hp, vec2 _pos, shared_ptr<Spritesheet> _spritesheet );
	~Zombie();

	// Dynamic is an abstract class
	bool update( Engine&, i64 ) override;

	void collision_callback( Collision& col, bool moving ) override;

	void death_callback( World& world ) override;

	// Drawable defaults are enough
private:
	i64 cooldown = 0;
};
