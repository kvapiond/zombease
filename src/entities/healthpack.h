
#pragma once

#include "prelude.h"
#include "entities/pawn.h"
#include "graphics/texture.h"
#include "physics/collision.h"
#include "graphics/spritesheet.h"
class Pickup;
#include "entities/pickup.h"

/// A pickup which heals the player.
class HealthPack : public Pickup {
public:
	static Graphics::Texture heart;

	HealthPack( f32 hp, vec2 _pos );

	void collision_callback( Collision& col, bool actor ) override;

private:
	f32 value;
};
