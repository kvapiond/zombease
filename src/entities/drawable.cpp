
#include "entities/drawable.h"

Drawable::Drawable( const Rectangle& rect )
: GameObject( rect ) {}

Drawable::Drawable( f32 _x, f32 _y, shared_ptr<Spritesheet> _spritesheet )
: GameObject( _x, _y, 0, 0 ), spritesheet( _spritesheet ) {
	dim = spritesheet->get_dim();
}

void Drawable::pretty_print( ostream& stream ) const {
	stream << "Drawable (";
	GameObject::pretty_print( stream );
	stream << ")";
}

bool Drawable::draw( Graphics::Renderer& target ) const {
	return draw( target, pos );
}

bool Drawable::draw( Graphics::Renderer& target, vec2 _pos ) const {
	spritesheet->render_to( target, _pos );
	return true;
}
