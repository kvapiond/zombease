
#pragma once

#include "prelude.h"
#include "math/vec2.h"
#include "utils/logger.h"
#include "utils/pretty.h"
#include "graphics/sdlwrapper.h"

namespace Graphics {
	namespace SDLWrapped {
		typedef SDLWrapper<SDL_Texture, SDL_DestroyTexture> Texture;
	}

	class Texture : public SDLWrapped::Texture, public Pretty {
	public:
		Texture( SDL_Texture* tex, string _path = "<unknown>" );
		virtual ~Texture() = default;

		void pretty_print( ostream& stream ) const override;

		vec2 get_dim() const noexcept;

	private:
		string path;
		vec2 dim;
	};
}
