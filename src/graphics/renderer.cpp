
#include "graphics/renderer.h"

using namespace Graphics;

Renderer Renderer::main = Renderer( nullptr );

Renderer::Renderer( SDL_Renderer* renderer )
: SDLWrapped::Renderer( renderer ), brush_colour( Colour::white ) {
	set_brush_colour_from_sdl_value();
}

Renderer::Renderer( Window& _win, bool vsync )
: SDLWrapped::Renderer( nullptr ), win( &_win ), brush_colour( Colour::white ) {
	let flags = (ue32)( SDL_RENDERER_ACCELERATED | ( vsync ? SDL_RENDERER_PRESENTVSYNC : 0x0 ) );

	/// @fixme this too (and **fix the one with description LAST**)
	raw_resource = make_shared(
		SDL_CreateRenderer( win->unpack(), -1, flags )
	);

	if ( raw_resource == nullptr ) {
		Logger::line() << ERROR << "Failed to create renderer:" << SDL_GetError();
	}

	set_draw_blend_mode( SDL_BLENDMODE_BLEND, true );
	set_brush_colour_from_sdl_value();
}

Renderer& Renderer::present() {
	SDL_RenderPresent( unpack() );
	return *this;
}

Renderer& Renderer::clear( Colour colour ) {
	set_brush_colour( colour );
	SDL_RenderClear( unpack() );
	return *this;
}

Colour Renderer::set_brush_colour( Colour colour, bool force ) {
	let prev_colour = brush_colour;

	if ( brush_colour != colour || force ) {
		if ( SDL_SetRenderDrawColor( unpack(), colour.r, colour.g, colour.b, colour.a ) != 0 ) {
			Logger::line() << ERROR << "Failed to change renderer draw colour:" << SDL_GetError();
		} else {
			brush_colour = colour;
		}
	}

	return prev_colour;
}

Renderer& Renderer::set_brush_colour_from_sdl_value() {
	u8 r, g, b, a;
	SDL_GetRenderDrawColor( unpack(), &r, &g, &b, &a );
	brush_colour = Colour( r, g, b, a );

	return *this;
}

Renderer& Renderer::draw_point( vec2 point, Colour colour ) {
	let prev_colour = set_brush_colour( colour );

	SDL_RenderDrawPoint( unpack(), (int)point.x, (int)point.y );

	set_brush_colour( prev_colour );
	return *this;
}

Renderer& Renderer::draw_rect( const Rectangle& rect, Colour colour, bool filled ) {
	let prev_colour = set_brush_colour( colour );
	let sdl_rect = (SDL_Rect)rect;

	if ( filled ) {
		SDL_RenderFillRect( unpack(), &sdl_rect );
	} else {
		SDL_RenderDrawRect( unpack(), &sdl_rect );
	}

	set_brush_colour( prev_colour );
	return *this;
}

Renderer& Renderer::draw_line( vec2 from, vec2 to, Colour colour ) {
	let prev_colour = set_brush_colour( colour );

	SDL_RenderDrawLine( unpack(), (int)from.x, (int)from.y, (int)to.x, (int)to.y );

	set_brush_colour( prev_colour );
	return *this;
}

Renderer& Renderer::draw_texture( Rectangle src_rect, Rectangle dst_rect, Texture texture ) {
	let sdl_src = (SDL_Rect)src_rect;
	let sdl_dst = (SDL_Rect)dst_rect;

	if ( SDL_RenderCopy( unpack(), texture.unpack(), &sdl_src, &sdl_dst ) != 0 ) {
		Logger::line() << ERROR << "Failed to render texture:" << SDL_GetError();
	}

	return *this;
}

Renderer& Renderer::draw_texture( vec2 pos, Texture texture, f32 scale ) {
	let src_rect = Rectangle( vec2::zero, texture.get_dim() );
	let dst_rect = Rectangle( pos, texture.get_dim() * scale );

	return draw_texture( src_rect, dst_rect, texture );
}

Renderer& Renderer::set_draw_blend_mode( SDL_BlendMode mode, bool force ) {
	if ( blend_mode != mode || force ) {
		if ( SDL_SetRenderDrawBlendMode( unpack(), mode ) != 0 ) {
			Logger::line() << ERROR << "Failed to set renderer draw blend mode:" << SDL_GetError();
		} else {
			blend_mode = mode;
		}
	}

	return *this;
}

Texture Renderer::create_texture_from_surface( Surface surface ) {
	return SDL_CreateTextureFromSurface( unpack(), surface.unpack() );
}

Texture Renderer::create_texture_from_surface( Surface surface, const string& path ) {
	return Texture( SDL_CreateTextureFromSurface( unpack(), surface.unpack() ), path );
}
