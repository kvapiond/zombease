
#include "graphics/texture.h"

using namespace Graphics;

Texture::Texture( SDL_Texture* tex, string _path )
: SDLWrapped::Texture( tex ), path( _path ) {
	if ( tex != nullptr ) {
		int w, h;
		if ( SDL_QueryTexture( unpack(), nullptr, nullptr, &w, &h ) != 0 ) {
			Logger::line() << ERROR << "Failed to read texture dimensions:" << SDL_GetError();
		} else {
			dim = vec2( (f32)w, (f32)h );
		}
	}
}

void Texture::pretty_print( ostream& stream ) const {
	stream << "Texture(\"" << path << "\", " << dim.x << "x" << dim.y << ")";
}

vec2 Texture::get_dim() const noexcept {
	return dim;
}
