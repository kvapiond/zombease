
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"

template<class T, void ( *deleter )( T* )>
class SDLWrapper {
public:
	/// Deleting the implicit constructor means the implementing class
	/// has to be instantiated using an existing T instance, or a
	/// nullptr. Being strict about initialisation is good, this way
	/// it's harder to leave a nullptr somewhere.
	SDLWrapper() = delete;

	inline SDLWrapper( T* raw )
	: raw_resource( make_shared( raw ) ) {}

	inline bool is_null() const {
		return raw_resource == nullptr;
	}

	inline T* unpack() {
		return &*raw_resource;
	}

protected:
	shared_ptr<T> raw_resource;

	inline static shared_ptr<T> make_shared( T* ptr ) {
		return shared_ptr<T>( ptr, deleter );
	}
};
