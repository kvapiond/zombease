
#include "graphics/window.h"

using namespace Graphics;

Window::Window( SDL_Window* win )
: SDLWrapped::Window( win ), rectangle( Rectangle::zero ) {
	if ( win != nullptr ) {
		int x = 0, y = 0, w = 0, h = 0;
		SDL_GetWindowPosition( win, &x, &y );
		SDL_GetWindowSize( win, &w, &h );
		rectangle = Rectangle( (f32)x, (f32)y, (f32)w, (f32)h );
	}
}

Window::Window( const string& _title, Rectangle _rect )
: SDLWrapped::Window( nullptr ), title( _title ), rectangle( _rect ) {
	/// @fixme I'm afraid this will not work -- is it really calling the special
	/// Graphics::SDLWrapper::make_shared()
	raw_resource = make_shared( SDL_CreateWindow(
		title.c_str(),
		(int)rectangle.x,
		(int)rectangle.y,
		(int)rectangle.w,
		(int)rectangle.h,
		SDL_WINDOW_SHOWN
	) );

	if ( raw_resource == nullptr ) {
		Logger::line() << ERROR << "Failed to create a window:" << SDL_GetError();
	}
}
