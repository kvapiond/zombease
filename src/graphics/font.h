
#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "prelude.h"

#include "utils/logger.h"

#include "graphics/colour.h"
#include "graphics/surface.h"
#include "graphics/sdlwrapper.h"

namespace Graphics {
	namespace SDLWrapped {
		typedef SDLWrapper<TTF_Font, TTF_CloseFont> Font;
	}

	class Font : public SDLWrapped::Font {
		using SDLWrapped::Font::SDLWrapper;
	public:
		Font( TTF_Font* raw );

		/// Load a TTF font from the given (path to a) file.
		static Font load( const string& path, u8 font_size = 16 );

		/// Render text in UTF8 to a new Graphics::Surface.
		Surface render_utf8_blended( const string& text, Colour colour ) const;
	};
}
