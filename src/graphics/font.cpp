
#include "graphics/font.h"

using namespace Graphics;

Font::Font( TTF_Font* raw )
: SDLWrapped::Font( raw ) {}

Font Font::load( const string& path, u8 font_size ) {
	let raw_font = TTF_OpenFont( path.c_str(), font_size );

	if ( raw_font == nullptr ) {
		Logger::line() << ERROR << "Failed to load TrueType font:" << TTF_GetError();
	}

	return raw_font;
}

Surface Font::render_utf8_blended( const string& text, Colour colour ) const {
	return TTF_RenderUTF8_Blended( &*raw_resource, text.c_str(), colour );
}
