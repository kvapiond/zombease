
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"
#include "math/vec2.h"
#include "utils/logger.h"
#include "math/rectangle.h"
#include "graphics/colour.h"
#include "graphics/window.h"
#include "graphics/texture.h"
#include "graphics/surface.h"
#include "graphics/sdlwrapper.h"

namespace Graphics {
	namespace SDLWrapped {
		typedef SDLWrapper<SDL_Renderer, SDL_DestroyRenderer> Renderer;
	}

	class Renderer : public SDLWrapped::Renderer {
	public:
		static Renderer main;

		Renderer( SDL_Renderer* renderer );
		Renderer( Window& _win, bool vsync = false );

		Renderer& present();
		Renderer& clear( Colour colour );
		Colour set_brush_colour( Colour colour, bool force = false );
		Renderer& set_brush_colour_from_sdl_value();
		Renderer& draw_rect( const Rectangle& rect, Colour colour, bool filled = false );
		Renderer& draw_point( vec2 pos, Colour colour );
		Renderer& draw_line( vec2 from, vec2 to, Colour colour );

		Renderer& draw_texture( Rectangle src_rect, Rectangle dst_rect, Texture texture );
		Renderer& draw_texture( vec2 pos, Texture texture, f32 scale = 1.0f );
		Renderer& draw_texture_rotated( vec2 pos, f32 angle, Texture texture );

		Renderer& set_draw_blend_mode( SDL_BlendMode mode, bool force = false );

		Texture create_texture_from_surface( Surface surface );
		Texture create_texture_from_surface( Surface surface, const string& path );

	private:
		/// @fixme ugly pointer!
		Window* win = nullptr;
		SDL_BlendMode blend_mode = SDL_BLENDMODE_BLEND;
		Colour brush_colour;
	};
}
