
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"
#include "utils/logger.h"
#include "math/rectangle.h"
#include "graphics/sdlwrapper.h"

namespace Graphics {
	namespace SDLWrapped {
		typedef SDLWrapper<SDL_Window, SDL_DestroyWindow> Window;
	}

	class Window : public SDLWrapped::Window {
	public:
		Window( SDL_Window* win );
		Window( const string& _title, Rectangle _rect );

	private:
		string title;
		Rectangle rectangle;
	};
}
