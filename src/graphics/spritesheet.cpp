
#include "graphics/spritesheet.h"

Spritesheet::Spritesheet( const string& _name, f32 _scale )
: name( _name ), scale( _scale ) {}

Spritesheet::Spritesheet( const string& _name, Graphics::Texture _texture, f32 _scale )
: Spritesheet( _name, _scale ) {
	dimensions = _texture.get_dim();
	frames.push_back( _texture );
}

void Spritesheet::render_to( Graphics::Renderer& renderer, vec2 pos ) const {
	for ( let& texture : frames ) {
		renderer.draw_texture( pos, texture, scale );
	}
}

Spritesheet& Spritesheet::resize( f32 new_scale ) {
	scale = new_scale;
	dimensions *= new_scale;

	return *this;
}

vec2 Spritesheet::get_dim() const {
	return dimensions;
}

void Spritesheet::pretty_print( ostream& stream ) const {
	stream
		<< "Spritesheet(" << name << ", "
		<< "dimensions = " << dimensions << ", "
		<< "#frames = " << frames.size() << ", ";

	/// @fixme u8?
	for ( u8 i = 0; i < frames.size(); i++ ) {
		stream << "\"" << frames[ i ];

		if ( i == frames.size() - 1 ) {
			stream << "\")";
		} else {
			stream << "\", \"";
		}
	}
}
