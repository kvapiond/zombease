
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"

namespace Graphics {
	struct Colour {
		static Colour white;
		static Colour red;
		static Colour green;
		static Colour blue;
		static Colour black;

		u8 r;
		u8 g;
		u8 b;
		u8 a;

		Colour( u8 _r, u8 _g, u8 _b, u8 _a );

		operator SDL_Colour();

		inline bool operator==( const Colour& other ) {
			return r == other.r && g == other.g && b == other.b && a == other.a;
		}

		inline bool operator!=( const Colour& other ) {
			return !( *this == other );
		}

		inline Colour operator*( f32 alpha ) const {
			return Colour( r, g, b, (u8)( alpha * a ) );
		}
	};
}
