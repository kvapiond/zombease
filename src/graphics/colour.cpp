
#include "graphics/colour.h"

using namespace Graphics;

Colour Colour::white( 0xff, 0xff, 0xff, 0xff );
Colour Colour::red  ( 0xff, 0x00, 0x00, 0xff );
Colour Colour::green( 0x00, 0xff, 0x00, 0xff );
Colour Colour::blue ( 0x00, 0x00, 0xff, 0xff );
Colour Colour::black( 0x00, 0x00, 0x00, 0xff );

Colour::Colour( u8 _r, u8 _g, u8 _b, u8 _a )
: r( _r ), g( _g ), b( _b ), a( _a ) {}

Colour::operator SDL_Colour() {
	SDL_Colour sdl_colour;

	sdl_colour.r = r;
	sdl_colour.g = g;
	sdl_colour.b = b;
	sdl_colour.a = a;

	return sdl_colour;
}
