
#pragma once

#include "prelude.h"
#include "math/vec2.h"
#include "utils/pretty.h"
#include "graphics/texture.h"
#include "graphics/renderer.h"

/// A sequence of related textures.
/// Typically used for animations.
/// @todo document this
class Spritesheet : public Pretty {
public:
	Spritesheet( const string&, f32 = 1.0 );
	Spritesheet( const string&, Graphics::Texture, f32 = 1.0 );
	virtual ~Spritesheet() = default;

	void render_to( Graphics::Renderer& renderer, vec2 pos ) const;
	Spritesheet& resize( f32 new_scale );

	vec2 get_dim() const;

	void pretty_print( ostream& ) const;

	friend ostream& operator<<( ostream&, const Spritesheet& );

private:
	vec2 dimensions;

	string name;
	f32 scale;

	vector<Graphics::Texture> frames;
};
