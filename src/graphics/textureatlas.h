
#pragma once

#include <SDL2/SDL_image.h>

#include "prelude.h"

#include "utils/logger.h"
#include "graphics/texture.h"
#include "graphics/surface.h"
#include "graphics/renderer.h"

/// Manages texture loading and caching.
class TextureAtlas {
public:
	TextureAtlas( Graphics::Renderer& );

	/// @todo shared_ptr?
	Graphics::Texture load_texture( const string& );

private:
	map<string, Graphics::Texture> textures;
	Graphics::Renderer& renderer;
};
