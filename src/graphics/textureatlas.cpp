
#include "graphics/textureatlas.h"

TextureAtlas::TextureAtlas( Graphics::Renderer& _renderer )
: renderer( _renderer ) {}

/// @todo Needs refactoring to move from SDL functions to our abstractions
Graphics::Texture TextureAtlas::load_texture( const string& path ) {
	SCOPE( path );

	if ( !textures.count( path ) ) {
		Logger::log.scope( "load" ) << ( "Texture '" + path + "' not loaded, loading" );

		using namespace Graphics;
		Surface image = IMG_Load( path.c_str() );

		if ( image.is_null() ) {
			/// @todo does Graphics::Texture handle these errors?
			Logger::line() << ERROR << "Loading failed:" << IMG_GetError() << END_SCOPE << END_SCOPE;
			return Texture( nullptr );
		}

		Logger::log << "Image loaded, creating a Texture";

		Texture texture = renderer.create_texture_from_surface( image, path );
		// SDL_Texture* texture_ptr = SDL_CreateTextureFromSurface( &*renderer, image );

		if ( texture.is_null() ) {
			Logger::line() << ERROR << "Texture conversion failed:" << IMG_GetError() << END_SCOPE << END_SCOPE;
			return Texture( nullptr );
		}

		textures.insert( make_pair( path, texture ) );

		Logger::log << END_SCOPE << END_SCOPE;
		return texture;
	}

	Logger::log << "Known texture, returning" << END_SCOPE;
	return textures.at( path );
}

