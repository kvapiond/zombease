
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"
#include "graphics/sdlwrapper.h"

namespace Graphics {
	namespace SDLWrapped {
		typedef SDLWrapper<SDL_Surface, SDL_FreeSurface> Surface;
	}

	class Surface : public SDLWrapped::Surface {
		using SDLWrapped::Surface::SDLWrapper;
	public:
	};
}
