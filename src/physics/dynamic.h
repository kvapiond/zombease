
#pragma once

#include <SDL2/SDL.h>

#include "prelude.h"

class World;
#include "utils/logger.h"
#include "entities/drawable.h"
#include "logic/world.h"
#include "math/geometry.h"
#include "math/rectangle.h"
#include "physics/collision.h"
#include "graphics/spritesheet.h"
#include "graphics/renderer.h"
#include "graphics/colour.h"

class Player;
class Engine;

/// A Drawable that can move around.
/// Actually, a Drawable can move around on its own. Dynamic objects are, however,
/// updated every frame.
class Dynamic : public Drawable, public enable_shared_from_this<Dynamic> {
public:
	static constexpr f32 MIN_SEPARATION = 0.01f;

	Dynamic( const Rectangle& rect );
	Dynamic( vec2 _pos, shared_ptr<Spritesheet> _spritesheet );
	Dynamic( f32 _x, f32 _y, shared_ptr<Spritesheet> _spritesheet );
	virtual ~Dynamic() = default;

	void pretty_print( ostream& stream ) const override;

	/// Update this dynamic game object.
	/// @param engine The engine instance for things like input controllers and collision resolution
	/// @param delta_time Time since last update (frame)
	/// @return true if the object should be kept alive, false otherwise
	virtual bool update( Engine& engine, i64 delta_time ) important = 0;

	/// Check whether it is meaningful to keep this dynamic object in memory.
	virtual bool alive() const important = 0;

	/// Return a set of event types that this object should listen for.
	virtual set<u32> get_registered_events() const important;

	/// Process an SDL_Event.
	virtual void handle( const SDL_Event& );

	/// Move to a set location, reacting to collisions.
	/// This method performs collision detection and resolution.
	/// For details on how this is done, see
	/// [the associated document](@ref md_src_docs_collisions)
	/// on this topic.
	/// @param world The world the Dynamic resides in
	/// @param target The coordinates it is trying to reach
	/// @return A pair of a boolean indicating whether the target location was reached,
	///         and a vector of pointers to Drawables the Dynamic collided with
	bool hot move_to( World& world, vec2 target ) override;
};
