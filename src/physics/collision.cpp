
#include "physics/collision.h"

CollisionData::CollisionData(
	shared_ptr<GameObject> _object,
	shared_ptr<GameObject> _other,
	i64 _timestamp,
	bool _overlap,
	f32 _travelled_ratio,
	vec2 _goal,
	vec2 _delta,
	vec2 _normal,
	vec2 _touch,
	Rectangle _object_rect,
	Rectangle _other_rect
) : object( _object )
  , other( _other )
  , timestamp( _timestamp )
  , overlap( _overlap )
  , travelled_ratio( _travelled_ratio )
  , goal( _goal )
  , delta( _delta )
  , normal( _normal )
  , touch( _touch )
  , object_rect( _object_rect )
  , other_rect( _other_rect ) {}


Collision::Collision( CollisionData _data )
: data( _data ) {}

// Slide
// if ( n_x == 0.0f ) {
// 	react_x = off_x;
// 	react_y = 0.0f;
// } else {
// 	react_x = 0.0f;
// 	react_y = off_y;
// }

// Redirect
// let mag = sqrt( off_x * off_x + off_y * off_y );
// if ( n_x == 0.0f ) {
// 	react_x = ( off_x > 0.0f ) ? mag : -mag;
// 	react_y = 0.0f;
// } else {
// 	react_x = 0.0f;
// 	react_y = ( off_y > 0.0f ) ? mag : -mag;
// }

// Bounce
// if ( n_x == 0.0f ) {
// 	react_x = +( target_x - line_x );
// 	react_y = -( target_y - line_y );
// } else {
// 	react_x = -( target_x - line_x );
// 	react_y = +( target_y - line_y );
// }

vec2 Collision::redirect( f32 resistance ) const {
	if ( data.normal.x == 0.0f ) {
		return data.touch + vec2(
			resistance * data.delta.magnitude(),
			0.0f
		);
	} else {
		return data.touch + vec2(
			0.0f,
			resistance * data.delta.magnitude()
		);
	}
}

vec2 Collision::bounce( f32 bounciness ) const {
	if ( data.normal.x == 0.0f ) {
		return data.touch + bounciness * vec2(  data.delta.x, -data.delta.y );
	} else {
		return data.touch + bounciness * vec2( -data.delta.x,  data.delta.y );
	}
}

vec2 Collision::slide( f32 resistance ) const {
	if ( data.normal.x == 0.0f ) {
		return data.touch + vec2( resistance * data.delta.x, 0.0f );
	} else {
		return data.touch + vec2( 0.0f, resistance * data.delta.y );
	}
}

vec2 Collision::ignore() const {
	return data.touch + data.delta;
}

vec2 Collision::cross() const {
	return ignore();
}

vec2 Collision::resolve() {
	/// @todo anything else?
	data.object->collision_callback( *this, true  );
	swap( data.object, data.other );
	data. other->collision_callback( *this, false );
	swap( data.object, data.other );

	data.object->set_pos( data.touch );

	return respond( data.object->collision_filter( *data.other ) );
}

vec2 Collision::respond( CollisionFilter filter ) const {
	switch ( filter ) {
		case CollisionFilter::Slide:
			return slide();
		case CollisionFilter::Bounce:
			return bounce();
		case CollisionFilter::Redirect:
			return redirect();
		case CollisionFilter::Ignore:
			return ignore();
		case CollisionFilter::Cross:
			return cross();
		default:
			Logger::log << ERROR << "Unknown filter kind";
			return ignore();
	}
}
