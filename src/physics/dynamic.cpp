
#include "physics/dynamic.h"

Dynamic::Dynamic( const Rectangle& rect )
: Drawable( rect ) {
	is_solid = true;
}

Dynamic::Dynamic( vec2 _pos, shared_ptr<Spritesheet> _spritesheet )
: Dynamic( _pos.x, _pos.y, _spritesheet ) {}

Dynamic::Dynamic( f32 _x, f32 _y, shared_ptr<Spritesheet> _spritesheet )
: Drawable( _x, _y, _spritesheet ) {
	is_solid = true;
}

void Dynamic::pretty_print( ostream& stream ) const {
	stream << "Dynamic (";
	Drawable::pretty_print( stream );
	stream << ")";
}

set<u32> Dynamic::get_registered_events() const {
	return set<u32>();
}

void Dynamic::handle( const SDL_Event& ) {}

/// Find the normal of a collision surface together with the fraction of
/// goal distance travelled.
/// @param[in] box The Minkowski rectangle used when detecting the collision
/// @param[in] goal The goal position
/// @param[out] min_fraction The fraction of goal distance travelled
/// @return The normal vector of the collision surface
vec2 get_intersection_normal( const Rectangle& box, const vec2& goal, f32& min_fraction ) {
	using namespace Geometry;

	let mut normal = vec2::zero;
	let mut n = vec2::zero;

	let a = get_lines_intersection_fraction(
		vec2::zero, goal,
		vec2( box.x2, box.y ), vec2( box.x, box.y ), normal
	);

	min_fraction = a;

	let b = get_lines_intersection_fraction(
		vec2::zero, goal,
		vec2( box.x2, box.y2 ), vec2( box.x2, box.y ), n
	);

	if ( b < min_fraction ) { min_fraction = b; normal = n; }

	let c = get_lines_intersection_fraction(
		vec2::zero, goal,
		vec2( box.x, box.y2 ), vec2( box.x2, box.y2 ), n
	);

	if ( c < min_fraction ) { min_fraction = c; normal = n; }

	let d = get_lines_intersection_fraction(
		vec2::zero, goal,
		vec2( box.x, box.y ), vec2( box.x, box.y2 ), n
	);

	if ( d < min_fraction ) { min_fraction = d; normal = n; }

	min_fraction = max( min( min_fraction, 1.0f ), -1.0f );

	return normal;
}

bool Dynamic::move_to( World& world, vec2 target ) {
	let this_shared = shared_from_this();

	for ( let chunk_ptr : world.get_overlapping_chunks( *this ) ) {
		if ( !chunk_ptr->remove( this_shared ) ) {
			Logger::log << ERROR << "Could not remove self from an overlapping chunk";
		}
	}

	vector<shared_ptr<Drawable>> intersected;

	// First of all, find all objects with which a collision is possible
	/// @todo Actually add all the chunks along the way, not just those we're intersecting
	set<shared_ptr<Drawable>> interesting_drawables;
	set<shared_ptr<WorldChunk>> interesting_chunks;

	vector<Collision> collisions;

	for ( let chunk_ptr : world.get_overlapping_chunks( *this ) ) {
		for ( let other_chunk_ptr : world.get_nearby_chunks( chunk_ptr, true ) ) {
			if ( interesting_chunks.find( other_chunk_ptr ) == interesting_chunks.end() ) {
				// cout << other_chunk_ptr->x << "x" << other_chunk_ptr->y << endl;
				interesting_chunks.insert( other_chunk_ptr );

				if ( !other_chunk_ptr->get_drawables().empty() ) {
					interesting_drawables.insert( other_chunk_ptr->get_drawables().begin(), other_chunk_ptr->get_drawables().end() );
				}
			}
		}
	}

	let mut moved = false;
	u8 counter = 0;

	do {
		moved = false;

		for ( let drawable_ptr : interesting_drawables ) {
			if ( drawable_ptr != this_shared
			&&   this_shared ->collision_filter( *drawable_ptr ) != CollisionFilter::Ignore
			&&   drawable_ptr->collision_filter( *this_shared  ) != CollisionFilter::Ignore
			) {
				using namespace Geometry;

				let drect = drawable_ptr->get_rect();
				let box = Rectangle::minkowski( drect, this->get_rect() );

				vec2 pen;
				box.closest_edge_projection( vec2::zero, pen );

				let goal = target - pos;

				if ( box.contains( vec2::zero ) ) {
					// Imminent collision, resolve immediately
					/// @todo actually wait, this should be different!
					// touch_pos is pos, but it doesn't change!
					// it's the correcting position (resulting destination)
					// which should be offset!
					let mut touch_pos = pos;
					moved = true;

					// intersected.push_back( drawable_ptr );

					touch_pos -= pen;

					// Ensure the objects are at least MIN_SEPARATION apart
					// if ( abs( pen.x ) < MIN_SEPARATION ) {
					// 	touch_pos.x = ( ( touch_pos.x > box.x + box.w / 2 ) ? +1 : -1 ) * MIN_SEPARATION;
					// }

					// if ( abs( pen.y ) < MIN_SEPARATION ) {
					// 	touch_pos.y = ( ( touch_pos.y > box.y + box.h / 2 ) ? +1 : -1 ) * MIN_SEPARATION;
					// }

					collisions.push_back( Collision(
						CollisionData(
							this_shared,
							drawable_ptr,
							time(),
							true,
							0.0f,
							goal,
							target - touch_pos,
							-pen.normalise(),
							/// @fixme is this an issue? I mean, we have to kinda guess where it was originally...
							// actually, see the above @todo
							touch_pos,
							this->get_rect(),
							drect
						)
					) );

					/// @fixme get rid of this >:(
					// if ( level < 10 ) {
					// 	return move_to( world, target, ++level );
					// }

				} else {
					// A collision could happen
					let mut frac = 0.0f;
					let normal = get_intersection_normal( box, goal, frac );

					/// @debug (as if it weren't obvious)
					using namespace Graphics;
					if ( id == 10 ) {
						Renderer::main.draw_line(
							drawable_ptr->get_pos() + drawable_ptr->get_dim() / 2,
							drawable_ptr->get_pos() + drawable_ptr->get_dim() / 2 + normal * 50.0f,
							Colour::blue
						);

						// Logger::line() << pos << frac;
					}

					let min_fraction = frac;

					if ( min_fraction != 1.0f ) {
						moved = true;
						let mut touch_pos = pos + goal * min_fraction;
						let mut new_touch = touch_pos;

						// let mut pen = vec2::zero;
						let box2 = Rectangle::minkowski( drect, Rectangle( touch_pos, dim ) );
						box2.closest_edge_projection( vec2::zero, pen );

						// if ( abs( pen.x ) < MIN_SEPARATION ) {
						// 	touch_pos.x += ( ( touch_pos.x > box.x + box.w / 2 ) ? +1 : -1 ) * MIN_SEPARATION;
						// }

						// if ( abs( pen.y ) < MIN_SEPARATION ) {
						// 	touch_pos.y += ( ( touch_pos.y > box.y + box.h / 2 ) ? +1 : -1 ) * MIN_SEPARATION;
						// }

						Renderer::main.draw_line(
							dim / 2 + touch_pos,
							dim / 2 + touch_pos + ( new_touch - touch_pos ) * 10,
							Colour::white
						);

						collisions.push_back( Collision(
							CollisionData(
								this_shared,
								drawable_ptr,
								time(),
								false,
								min_fraction,
								goal,
								target - touch_pos,
								normal,
								touch_pos,
								this->get_rect(),
								drect
							)
						) );
					}
				}


				/// @todo verify MIN_SEPARATION here
			}
		}

		if ( !collisions.empty() ) {
			let mut min_ratio = inf;
			let mut& earliest_contact = *collisions.begin();

			for ( let_mut& collision : collisions ) {
				if ( collision.data.overlap ) {
					// all overlaps must be resolved
					// overlaps don't hold useful redirection information
					let __attribute__(( __unused__ )) foo = collision.resolve();
					break;
				} else {
					if ( collision.data.travelled_ratio < min_ratio ) {
						earliest_contact = collision;
						min_ratio = collision.data.travelled_ratio;
					}
				}
			}

			target = earliest_contact.resolve();

			let box = Rectangle::minkowski(
				earliest_contact.data. other->get_rect(),
				earliest_contact.data.object->get_rect()
			);

			if ( box.contains( vec2::zero ) ) {
				vec2 pen;
				box.closest_edge_projection( vec2::zero, pen );
				let mut obj_pos = earliest_contact.data.object->get_pos();

				if ( abs( pen.x ) < MIN_SEPARATION ) {
					obj_pos.x += ( ( obj_pos.x > box.x + box.w / 2 ) ? +1 : -1 ) * MIN_SEPARATION;
				} else if ( abs( pen.y ) < MIN_SEPARATION ) {
					obj_pos.y += ( ( obj_pos.y > box.y + box.h / 2 ) ? +1 : -1 ) * MIN_SEPARATION;
				}

				earliest_contact.data.object->set_pos( obj_pos );
			}
		}
	} while ( counter++ < 1 && ( moved && ( pos - target ).magnitude() > 0.01f ) );

	if ( !moved ) {
		pos = target;
	}

	for ( let chunk_ptr : world.get_overlapping_chunks( *this ) ) {
		if ( !chunk_ptr->add( this_shared ) ) {
			Logger::log << ERROR << "Could not add self to an overlapping chunk";
		}
	}

	return intersected.empty();
}
