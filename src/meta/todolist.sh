#!/bin/bash

SRC_PATH="$SRC_PATH"
if [[ -z "$SRC_PATH" ]]
then
	SRC_PATH=src
fi

files=$(find $SRC_PATH/ -type f)
matches=$(/bin/grep -ilE "@(todo|fixme|debug)" $(xargs <<< "$files"))

n=0
for file in $matches; do
	echo -e "\\033[1m$(dirname "$file")/\\033[93m$(basename "$file")\\033[0m:"
	results=$(/bin/grep -niE --colour=always "@(todo|fixme|debug)" "$file")
	n=$((n + $(wc -l <<< "$results")))
	sed -E "s/^([^\\t \\/]*)[\\t ]*(\\/\\/)/\\1\\t\\2/g" <<< "$results"
	echo ""
done

if [[ n -gt 0 ]]
then
	echo -e "\\033[1mFound \\033[33m$n\\033[0;1m todos/fixmes/debugs in $(wc -l <<< \"$matches\") files\\033[0m"
fi
