#!/bin/bash

# A couple of simple sanity checks, useful as a git hook (pre-commit)

SRC_PATH="$SRC_PATH"
if [[ -z "$SRC_PATH" ]]
then
	SRC_PATH=src
fi

matches=$(/bin/grep -L "^#pragma once" $SRC_PATH/*.h $SRC_PATH/*/*.h)
success=0

warn() {
	echo -e "\\033[93mWarning: \\033[0m$1"
}

err() {
	echo -e "\\033[91mError: \\033[0m$1"
}

list() {
	echo -en "\\033[3$1m"
	sed "s/ /\\n/g" <<< "$matches" | sed "s/^/  /g"
	echo -en "\\033[0m"
}

if [[ ! -z "$matches" ]]
then
	err "Some headers could be included twice:"
	list 1
	success=1
fi

files=$(find $SRC_PATH/ -type f -name \*.h ! -path \*/tests/\* ! -name dummy.h ! -name prelude.h)
matches=$(/bin/grep -L "^#include \"prelude\\.h\"" $(xargs <<< "$files"))

if [[ ! -z "$matches" ]]
then
	err "Some headers do not include the prelude:"
	list 1
	success=1
fi

files=$(find $SRC_PATH/ -type f ! -path $SRC_PATH/graphics/\*)
matches=$(/bin/grep -l "^#include <SDL" $(xargs <<< "$files"))

if [[ ! -z "$matches" ]]
then
	warn "Some files outside the graphics/ directory depend on SDL:"
	list 3
fi

exit $success
