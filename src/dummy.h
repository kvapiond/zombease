
/// @file dummy.h
/// This file is never included from source code. It only contains
/// definitions and annotations to help Doxygen produce nicer outputs.

#pragma once

namespace std {
	/// This class helps Doxygen better understand shared_ptr<T>.
	/// It is not actually used anywhere in the codebase.
	template<typename T>
	class shared_ptr {
		T *value;
	};
}

/// @namespace Graphics
/// Contains computer graphics-specific classes,
/// especially abstractions over SDL constructs.
namespace Graphics {
	/// @namespace Graphics::SDLWrapped
	/// Stores aliases to SDLWrapper variations.
	/// Serves as a repository for type -- deleter pairs
	/// of SDL functions.
	namespace SDLWrapped {}
}
