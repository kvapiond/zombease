
### Configuration ###

SHELL = /bin/bash
BINARY = zombease
CXX ?= g++
EXTENSION = cpp
# Warning: do NOT add the trailing slash!
SRC_PATH = src

PKG_CONFIG_LIBS = sdl2

# -Wformat-overflow=2 and -Wnull-dereference are unavailable on the ProgTest image
COMPILE_FLAGS = -std=c++11 -Wall -Wextra -Wshadow -Wpointer-arith -Wunused-macros \
-Wstrict-overflow=5 -Wswitch-default -Wundef -Wcast-align -Wconversion \
-Wcast-qual -Wswitch -Wunreachable-code -Wwrite-strings -Winit-self -Wformat=2 \
-Wno-long-long -Wunused-result

COMPILE_FLAGS_DEBUG = -D CFG_DEBUG -Og -g3
COMPILE_FLAGS_RELEASE = -D CFG_RELEASE -Ofast -flto
COMPILE_FLAGS_PROGTEST = -D CFG_RELEASE -O0 -ggdb -g3

# Add additional include paths
# We obviously shouldn't include anything from BUILD_PATH,
# but this is necessary for #include "tests/all.h" to work.
INCLUDES = -I $(SRC_PATH) -I $(BUILD_PATH)

LINK_FLAGS = -std=c++11 -lSDL2_image -lSDL2_ttf -lm
LINK_FLAGS_DEBUG = -Og -fsanitize=address
LINK_FLAGS_RELEASE = -Ofast -flto
LINK_FLAGS_PROGTEST = -O0

DOXYGEN = doxygen
DOXYGEN_FLAGS =

# Can be emptied to show CXX invocations
AT_SYMBOL = @

USERNAME = kvapiond
ZIP_CONTENTS = src/ Makefile Doxyfile README.md

### Preprocessing ###

# Add g++-specific improvements
ifeq ($(CXX),g++)
	COMPILE_FLAGS         += -fdelete-null-pointer-checks
	COMPILE_FLAGS_RELEASE += -fno-fat-lto-objects
endif

# Enable colour output on GitLab CI
ifneq ($(CI),)
	COMPILE_FLAGS += -fdiagnostics-color=always
	LINK_FLAGS    += -fdiagnostics-color=always
endif

# Resolve pkg-config libraries
ifneq ($(PKG_CONFIG_LIBS),)
	COMPILE_FLAGS += $(shell pkg-config --cflags $(PKG_CONFIG_LIBS))
	LINK_FLAGS += $(shell pkg-config --libs $(PKG_CONFIG_LIBS))
endif

# Combine compiler and linker flags
debug:    export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(COMPILE_FLAGS_DEBUG)
debug:    export LDFLAGS  := $(LDFLAGS)  $(LINK_FLAGS)    $(LINK_FLAGS_DEBUG)
release:  export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(COMPILE_FLAGS_RELEASE)
release:  export LDFLAGS  := $(LDFLAGS)  $(LINK_FLAGS)    $(LINK_FLAGS_RELEASE)
progtest: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(COMPILE_FLAGS_PROGTEST)
progtest: export LDFLAGS  := $(LDFLAGS)  $(LINK_FLAGS)    $(LINK_FLAGS_PROGTEST)

# Source, build and output paths
simple-lint: export SRC_PATH   := $(SRC_PATH)
$(USERNAME): export BUILD_PATH := target/progtest
$(USERNAME): export BIN_PATH   := bin/progtest
progtest:    export BUILD_PATH := target/progtest
progtest:    export BIN_PATH   := bin/progtest
release:     export BUILD_PATH := target/release
release:     export BIN_PATH   := bin/release
debug:       export BUILD_PATH := target/debug
debug:       export BIN_PATH   := bin/debug
# TODO ----------------------->:  bin/?
doc:         export BUILD_PATH := bin/release
doc:         export BIN_PATH   := .

SOURCES = $(shell find $(SRC_PATH) -name '*.$(EXTENSION)' -printf '%T@\t%p\n' \
			| sort -k 1nr \
			| cut -f2-)

OBJECTS = $(SOURCES:$(SRC_PATH)/%.$(EXTENSION)=$(BUILD_PATH)/%.o)
DEPENDENCIES = $(OBJECTS:.o=.d)

# Collect test files. Will be used later to generate tests/all.h
ESCAPED_PATH = $(shell sed 's/\//\\\//g' <<< $(SRC_PATH))
TEST_INCLUDES = $(shell find '$(SRC_PATH)/tests/' -type f -name '*.h' ! -name 'all.h' ! -name 'test.h' ! -name 'all_template.h' \
	| sed 's/^$(ESCAPED_PATH)\//\#include </g' \
	| sed 's/h$$/h>/g' \
	| sed 's/\//\\\//g')

TEST_FILES = $(shell find $(SRC_PATH)/tests/ -type f -name '*.h' ! -name 'all.h')

ALL_H_TEMPLATE_PATH = $(SRC_PATH)/tests/all_template.h
ALL_H_PATH = $(BUILD_PATH)/tests/all.h


### Targets ###

.PHONY: all
all: compile doc

.PHONY: compile
compile: $(USERNAME)

.PHONY: progtest
progtest: dirs
	@echo "Beginning build for target $@"
	@$(MAKE) _all --no-print-directory
	@echo "Build finished"

.PHONY: release
release: dirs
	@echo "Beginning build for target $@"
	@# Hint: disable --no-print-directory to see what's happening
	@$(MAKE) _all --no-print-directory
	@echo "Build finished"

.PHONY: debug
debug: dirs
	@echo "Beginning build for target $@"
	@$(MAKE) _all --no-print-directory
	@echo "Build finished"

.PHONY: run
run: progtest
	@./bin/$</$(BINARY)

.PHONY: test
test: debug
	@./bin/$</$(BINARY) --test-only

.PHONY: run-dbg
run-dbg: debug
	@./bin/$</$(BINARY)

.PHONY: simple-lint
simple-lint:
	@./src/meta/simplelint.sh

$(USERNAME): progtest
	@cp $(BIN_PATH)/$(BINARY) $(USERNAME)

# Unfortunately this has to be .PHONY, because of the all.h dependency on main.cpp
.PHONY: $(USERNAME).zip
$(USERNAME).zip:
	@$(RM) -r $@ $(USERNAME)
	@mkdir -p $(USERNAME)/examples/
	@cp -r $(ZIP_CONTENTS) $(USERNAME)/
	@cp $(SRC_PATH)/docs/zadani.txt $(USERNAME)/
	@cp $(SRC_PATH)/docs/prohlaseni.txt $(USERNAME)/
	@zip -r $@ $(USERNAME)/
ifeq ($(CI),)
	@$(RM) -r $(USERNAME)
endif

.PHONY: test-zip
test-zip: $(USERNAME).zip
	@echo "Testing ProgTest zip"
	unzip -o $<
	[ -d $(USERNAME)/examples/ ]
	[ -s $(USERNAME)/zadani.txt ]
	[ -s $(USERNAME)/prohlaseni.txt ]
	cmp $(USERNAME)/zadani.txt $(SRC_PATH)/docs/zadani.txt
	cmp $(USERNAME)/prohlaseni.txt $(SRC_PATH)/docs/prohlaseni.txt
	cd $(USERNAME); make compile
	cd $(USERNAME); make doc
	cd $(USERNAME); make clean
	@echo "ProgTest zip test complete"

.PHONY: doc
doc:
	$(MAKE) doc-inner --no-print-directory

.PHONY: doc-inner
doc-inner: dirs $(SOURCES)
	@echo "Launching Doxygen"
	@$(DOXYGEN) $(DOXYGEN_FLAGS)

$(ALL_H_PATH): $(TEST_FILES)
	@echo "Generating: $@"
	@sed 's/\t<TEST_INCLUDES>/$(TEST_INCLUDES:#include=\n\t#include)/g' > $@ < $(ALL_H_TEMPLATE_PATH)

.PHONY: dirs
dirs:
	@echo "Creating directories"
	@mkdir -p $(dir $(OBJECTS))
	@mkdir -p $(BUILD_PATH)/tests/
	@mkdir -p $(BIN_PATH)

# Removes all build files
.PHONY: clean
clean:
	@echo "Deleting $(BINARY) symlink"
	@$(RM) $(BINARY)
	@echo "Deleting save file"
	@$(RM) zombease-save.txt
	@echo "Deleting zip"
	@$(RM) $(USERNAME).zip
	@echo "Deleting binary/zip workdir"
	@$(RM) -r $(USERNAME)
	@echo "Deleting directories"
	@$(RM) -r target
	@$(RM) -r bin
	@$(RM) -r doc

# Main rule, checks the executable and symlinks to the output
_all: $(BIN_PATH)/$(BINARY)
	@echo "Making symlink: $(BINARY) -> $<"
	@$(RM) $(BINARY)
	@ln -s $(BIN_PATH)/$(BINARY) $(BINARY)

# Link the executable
$(BIN_PATH)/$(BINARY): $(OBJECTS)
	@echo "Linking: $@"
	$(AT_SYMBOL)$(CXX) $(OBJECTS) $(LDFLAGS) -o $@

-include $(DEPENDENCIES)

$(BUILD_PATH)/%.o: $(SRC_PATH)/%.$(EXTENSION)
	@echo "Compiling: $< -> $@"
	$(AT_SYMBOL)$(CXX) $(CXXFLAGS) $(INCLUDES) -MP -MMD -c $< -o $@

# This is the only specific rule we need
$(SRC_PATH)/main.cpp: $(ALL_H_PATH)
